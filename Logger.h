/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "config.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#define LOG_(Logger_, Message_)                 \
  Logger_(                                      \
    dynamic_cast<std::ostringstream &>(         \
      std::ostringstream().flush() << Message_) \
      .str())
#define LOG(Message_) LOG_(GetLogger(), Message_)
#define LOG_VERBOSE(Message_) \
  if (GetLogger().getVerbose()) LOG_(GetLogger(), "[VERBOSE] " << Message_)

class Logger {
public:
  Logger() : printLog(true), printStdout(true), verbose(false){};
  void operator()(std::string const &message);
  void setPrintStdout(bool printStdout);
  void setPrintLog(bool printLog);
  void setVerbose(bool verbose);
  bool getVerbose();

private:
  bool printStdout;
  bool printLog;
  bool verbose;
};

Logger &GetLogger();
