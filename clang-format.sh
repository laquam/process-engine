#!/bin/bash
clang-format-12 -style=file -i $(find . \( -path ./lib -o -path ./cmake-build-debug \) -prune -false -o -name "*.cpp" -or -iname "*.h" -or -iname "*.hpp")
git diff --exit-code --ignore-submodules && echo "Code formatting OK"
