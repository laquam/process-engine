/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "Logger.h"
#include "ProcessEngine.h"
#include "util.h"
#include "xmlhelp.h"
#include <CLI/CLI.hpp>
#include <iostream>
#include <nlohmann/json.hpp>
#include <unistd.h>


using json = nlohmann::json;

static ProcessEngine processEngine;

void signal_handler(int signal) {
  processEngine.signalHandler(signal);
}

int main(int argc, char *argv[]) {
  signal(SIGTERM, signal_handler);

  CLI::App app{"Sequential Process Engine: Executes kadi workflows."};
  app.set_version_flag("--version", PROCESS_ENGINE_VERSION);
  app.add_flag_callback("--commands", [&]() {
    listSubCommands(&app, argv[0]);
    exit(0);
  });

  std::string workflowFile, path, inputValue;
  bool dryRun = false, detach = false, verbose = false;
  bool run_continue_option = false; // for backward-compatibility
  std::string interactionId;

  app.require_subcommand(1);
  auto startCommand = app.add_subcommand("start", "Run given workflow");
  startCommand->alias("run"); // for backward-compatibility
  startCommand->add_flag("-n,--dry", dryRun,
                         "Process the workflow but do not actually run any commands");
  startCommand->add_flag("-d,--detach", detach,
                         "Detach from the executing process. Display workflow info and exit");

  // for backward-compatibility
  startCommand->add_flag("-c,--continue", run_continue_option,
                         "Continue workflow execution after user interaction, signals the process engine process associated to the specified workflow to continue. DEPRECATED: Please use the separate subcommand 'continue' instead");

  startCommand->add_option("-p,--path", path,
                           "Path to the directory to execute the workflow in.")
    ->required(true);
  startCommand->add_option("file", workflowFile,
                           "Workflow description file (.flow)")
    ->required(true);
  startCommand->add_flag("-v,--verbose", verbose, "Print more information about internal events");

  auto continueCommand = app.add_subcommand("continue", "Continue workflow execution after user interaction.");
  auto statusCommand = app.add_subcommand("status", "Print the status of a workflow.");
  auto logCommand = app.add_subcommand("log", "Print the log of a workflow");
  auto shortcutsCommand = app.add_subcommand("shortcuts", "Print a list of the shortcuts of a workflow.");
  auto interactionsCommand = app.add_subcommand("interactions", "Print interaction descriptions");
  auto inputInteractionValueCommand = app.add_subcommand("input", "Store an input value for given interaction.");
  auto cancelCommand = app.add_subcommand("cancel", "Cancel workflow execution");

  for (auto command : {continueCommand, statusCommand, logCommand, shortcutsCommand, interactionsCommand, inputInteractionValueCommand, cancelCommand}) {
    command->add_option("-p,--path", path,
                        "Path to the workflow directory.")
      ->required(true);
    command->add_flag("-v,--verbose", verbose, "Print more information about internal events");
  }
  inputInteractionValueCommand->add_option("interactionId", interactionId, "Id of the workflow")->required(true);
  inputInteractionValueCommand->add_option("value", inputValue, "Value")->required();

  bool xmlhelp = false;
  for (char **pargv = argv + 1; *pargv != argv[argc]; pargv++) {
    if (*pargv) {
      std::string argument = std::string(*pargv);
      if (argument == "--xmlhelp") {
        xmlhelp = true;
        app.set_help_flag("--xmlhelp");
        break;
      }
    }
  }

  // the following is a modified version of what CLI11_PARSE(app, argc, argv) expands to, allowing the special parameters
  // `--xmlhelp` and `--commands` to ignore required options, similar as `--help`
  try {
    (app).parse((argc), (argv));
  } catch (const CLI::ParseError &e) {
    if (xmlhelp) {
      for (auto command : app.get_subcommands()) {
        if (command) {
          printXMLHelp(command, PROCESS_ENGINE_VERSION);
          exit(0);
        }
      }
      throw std::runtime_error("Error: Unable to print xmlhelp (XMLHelp is only available for subcommands. Use --commands to get a list of all subcommands)");
    }
    return (app).exit(e);
  }

  // disable printing to stdout in detach mode (standard pipes will be closed anyway in the child process)
  GetLogger().setPrintStdout(!detach);

  // set verbose if -v/--verbose is enabled
  GetLogger().setVerbose(verbose);

  // get absolute path for the workflow file before cd'ing into target folder
  std::string workflowFileAbsolute = workflowFile;
  if (!util::isAbsolutePath(workflowFile)) {
    workflowFileAbsolute = util::getAbsolutePath(workflowFile);
  }
  if (!util::fileExists(workflowFileAbsolute)) {
    std::cerr << "Error: Can not open workflow file: " << workflowFileAbsolute << std::endl;
    return 1;
  }

  // cd into target folder
  if (path.empty()) {
    std::cerr << "Error: Path is empty" << std::endl;
    exit(1);
  }
  if (chdir(path.c_str()) != 0) {
    std::cerr << "Error: Unable to change directory to " << path << std::endl;
    exit(1);
  }

  if (path.back() != '/') {
    path += '/';
  }

  processEngine.setPath(path);
  bool success = true;

  if (*startCommand) {
    if (run_continue_option) {
      // deprecated, only for backwards compatibility
      success = processEngine.continueWorkflow();
    } else {
      try {
        success = processEngine.runWorkflow(workflowFileAbsolute, dryRun, detach);
      } catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
      }
    }
  } else if (*continueCommand) {
    success = processEngine.continueWorkflow();
  } else if (*statusCommand) {
    try {
      processEngine.printWorkflowStatus();
    } catch (const std::exception &e) {
      std::cerr << "Error fetching status: " << e.what() << std::endl;
    }
  } else if (*logCommand) {
    processEngine.printWorkflowLog();
  } else if (*shortcutsCommand) {
    processEngine.printWorkflowShortcuts();
  } else if (*interactionsCommand) {
    ProcessEngine::printInteractions();
  } else if (*inputInteractionValueCommand) {
    success = ProcessEngine::inputInteractionValue(interactionId, inputValue);
  } else if (*cancelCommand) {
    success = processEngine.cancelWorkflow();
  }

  return success ? 0 : 1;
}
