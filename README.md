# process engine
This is a sequential workflow execution engine to execute kadi workflows.

## Download and install latest .deb package
```
wget https://gitlab.com/iam-cms/workflows/process-engine/-/jobs/artifacts/master/download?job=pack_deb -O process_engine.zip
unzip process_engine.zip
sudo apt install ./build/process_engine-[VERSION].deb
```

## How to build
```
git submodule update --init
mkdir build
cd build/
cmake ..
make
```


## Usage
Running a workflow:
```
process_engine start -p /tmp/my-execution-folder my-workflow.flow
```
More info can be obtained with `process_engine --help` and `process_engine [sub-command] --help`.

## Configuration for the process manager
This is the default configuration for the [process manager](https://gitlab.com/iam-cms/workflows/process-manager) to use this process engine:
```
{
  "engines": [
    {
      "name": "SequentialPE",
      "startCommand": "process_engine start -dp %s %s",
      "continueCommand": "process_engine continue -p %s",
      "statusCommand": "process_engine status -p %s",
      "logCommand": "process_engine log -p %s",
      "shortcutsCommand": "process_engine shortcuts -p %s",
      "interactionsCommand": "process_engine interactions -p %s",
      "inputInteractionValueCommand": "process_engine input -p %s %s %s"
    }
  ],
  "default_engine": "SequentialPE"
}
```

# Development

To also build the unit tests, set the cmake variable `build_test` to `ON` when generating the cmake cache (default is `OFF`):
```
cmake .. -Dbuild_tests=ON
```
