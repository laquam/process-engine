#!/bin/bash

mkdir -p build
( cd build && cmake .. )
( cd build && cmake --build . )
( cd build && cpack )

echo
echo "DEB content:"
echo "------------------------------"
# shellcheck disable=SC2012
DEB=$(ls -t build/*.deb | head -1)
dpkg -I "$DEB"
dpkg -c "$DEB"
