/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "BoundingBox.h"
#include <regex>

namespace data {
  void to_json(json &j, const BoundingBox &c) {
    j = json{{"x", c.x}, {"y", c.y}, {"width", c.width}, {"height", c.height}};
  }
  void from_json(const json &j, BoundingBox &c) {
    /* this would be the preferred implementation, but is not yet supported by Kadistudio:
    c.x = j["x"].get<int>();
    c.y = j["y"].get<int>();
    c.width = j["width"].get<int>();
    c.height = j["height"].get<int>();
    */
    auto boundingBoxString = j.get<std::string>();
    std::regex rx("^\\[([0-9]*), ([0-9]*), ([0-9]*), ([0-9]*)\\]");
    std::smatch matches;
    std::regex_match(boundingBoxString, matches, rx);
    if (matches.size() < 4) {
      throw std::logic_error("Could not parse crop selection: Invalid crop selection format, expected 4 numbers found " + std::to_string(matches.size()));
    }
    try {
      // matches.str(0) returns the whole string
      c.x = std::stoi(matches.str(1));
      c.y = std::stoi(matches.str(2));
      c.width = std::stoi(matches.str(3));
      c.height = std::stoi(matches.str(4));
    } catch (std::invalid_argument &exception) {
      throw std::logic_error("Could not parse crop selection: Number expected");
    } catch (std::out_of_range &exception) {
      throw std::logic_error("Could not parse crop selection: Value out of range");
    }
  }
} // namespace data
