/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "WorkflowExecutor.h"
#include "../Logger.h"
#include "../util.h"
#include "WorkflowAnalyzer.h"
#include "nodes/FileOutputNode.h"
#include "nodes/UserInteractionNode.h"
#include <iomanip>

bool WorkflowExecutor::execute(AbstractWorkflow *workflow, WorkflowExecution *execution, bool dryRun, bool detach, bool haltForInteraction) {
  bool success = true;
  bool execution_done = false;
  execution->setState(ExecutionState::RUNNING);
  auto nodes = workflow->getNodes();
  std::unordered_set<std::string> shortcuts;

  // important setup steps (order matters!)
  workflow->initCoherentGroups();
  WorkflowAnalyzer(workflow).generateSequentialExecutionSets(); // to detect circles, don't reuse this analyzer
  workflow->initBranches(execution);
  // end of setup

  WorkflowAnalyzer workflowAnalyzer(workflow);

  execution->persistInfo();
  std::string command;

  while (!execution_done) {
    printExecutionStartMessage(workflow, execution, dryRun);
    auto sequentialExecutionSets = workflowAnalyzer.generateSequentialExecutionSets();

    /* will be set to true if a user interaction is required. If true: Stop executing commands but process any
     * interaction nodes which can be reached without violating dependency relations */
    bool suspendForInteraction = false;
    for (const auto &nextNodeSet : sequentialExecutionSets) {

      // before each new node execution, check if execution must be cancelled
      if (execution->isCancelling()) {
        printCancelMessage(workflow);
        execution->setState(ExecutionState::CANCELLED);
        execution->persistInfo();
        return true;
      }

      std::ostringstream nodeStream;
      for (const auto &nextNodeId : nextNodeSet) {
        nodeStream << nextNodeId << " ";
      }
      LOG_VERBOSE("NodeSet " << nodeStream.str());
      for (const auto &nextNodeId : nextNodeSet) {
        auto nodeItem = nodes.find(nextNodeId);
        if (nodeItem != nodes.end()) {
          const auto &node = nodeItem->second;

          if (!suspendForInteraction) {
            if (execution->nodeStillToProcess(node->getId())) {
              // execution step
              printNodeExecutionHeader(workflow, execution, node);

              success &= node->processInputPortData(execution);
              if (success)
                success &= node->execute(execution, dryRun, detach, execution->getIteration());
              if (success)
                success &= node->processOutputPortData(execution);
            } else if (execution->nodeNeedsPostExecution(node->getId())) {
              node->postExecutionHook(execution, dryRun, detach, execution->getIteration());
            }
            if (success) {
              if (execution->getNodeState(node->getId()).needsInteraction()) {
                suspendForInteraction = true;
                printSuspendingMessage(workflow);
              }
              execution->persistInfo();
            }

            /* register the shortcut after the node was executed, because the node might update itself when executing */
            auto fileOutputNode = dynamic_cast<FileOutputNode *>(node);
            if (fileOutputNode && fileOutputNode->hasShortcut()) {
              shortcuts.insert(fileOutputNode->getShortcut(execution));
            }

            if (!success) {
              execution->setState(ExecutionState::ERROR);
              execution->persistInfo();
              return false;
            }
          } else {
            // finish processing of all _reachable_ interaction nodes
            auto userInteractionNode = dynamic_cast<UserInteractionNode *>(node);
            if (userInteractionNode) {
              WorkflowNodeStates *nodeStates = execution->getNodeStates();
              if (workflowAnalyzer.countUnresolvedDependencies(nodeStates, userInteractionNode->getId()) == 0) {
                if (execution->nodeStillToProcess(userInteractionNode->getId())) {
                  LOG_VERBOSE("Processing reachable interaction node " << userInteractionNode->getName());
                  userInteractionNode->processInputPortData(execution);
                  userInteractionNode->execute(execution, dryRun, detach, execution->getIteration());
                } else {
                  LOG_VERBOSE("Reachable interaction node " << userInteractionNode->getName()
                                                            << " already processed.");
                }
              } else {
                LOG_VERBOSE("Skipping unreachable interaction node " << userInteractionNode->getName());
              }
            }
          }
        } else {
          LOG("Warning: Tried to execute Node with id " << nextNodeId << ", but did not find it in the workflow!");
        }
      }
    }

    // code from here is only reached when (success == true)
    if (suspendForInteraction) {
      execution->setState(ExecutionState::NEEDS_INTERACTION);
      execution->persistInfo();

      if (!haltForInteraction) {
        // set up the signal set which defines the signals the process should wait for.
        sigemptyset(&mysigset);
        sigaddset(&mysigset, SIGUSR1);

        // stop process execution by waiting for SIGUSR1
        sigprocmask(SIG_BLOCK, &mysigset, &myoldsigset);
        siginfo_t siginfo;
        sigwaitinfo(&mysigset, &siginfo);
        sigprocmask(SIG_UNBLOCK, &mysigset, nullptr);
        LOG("** Resuming execution after user interaction **");

        execution->setState(ExecutionState::RUNNING);
        execution->setIteration(execution->getIteration() + 1);
        execution->persistInfo();
      } else {
        execution_done = true;
      }

    } else {
      execution->setState(ExecutionState::FINISHED);
      execution_done = true;

      // create shortcuts
      json jsonObject;
      jsonObject["shortcuts"] = json::array();
      for (const auto &relativePath : shortcuts) {
        std::string absolutePath = util::getAbsoluteFilePath(relativePath);
        json shortcutJson;
        shortcutJson["path"] = absolutePath;
        shortcutJson["name"] = relativePath;
        jsonObject["shortcuts"].push_back(shortcutJson);
      }
      std::ofstream out(config::SHORTCUT_FILENAME);
      out << std::setw(4) << jsonObject << std::endl;
    }
  }

  execution->persistInfo();
  printExecutionResult(workflow, execution);
  return success;
}

void WorkflowExecutor::printExecutionStartMessage(const AbstractWorkflow *workflow, const WorkflowExecution *execution,
                                                  bool dryRun) {
  bool currentlyInBranch = isBranch(workflow);
  std::string workflowType = currentlyInBranch ? "BRANCH" : "WORKFLOW";
  printLogSeparator(true);
  LOG(std::endl
      << workflowType << " EXECUTION");
  if (dryRun) {
    LOG(" (dry run, only printing commands)");
  }
  printLogSeparator(true);
}

void WorkflowExecutor::printNodeExecutionHeader(const AbstractWorkflow *workflow, const WorkflowExecution *execution,
                                                const Node *node) {
  std::string branchInfo = isBranch(workflow) ? "in branch: " : "";
  LOG(std::endl);
  LOG("[" << branchInfo << execution->getNodesProcessed() + 1 << "/" << execution->getNodesToProcess()
          << "] Executing " << node->getName() << " " << node->getId() << ": ");
  printLogSeparator();
}

bool WorkflowExecutor::isBranch(const AbstractWorkflow *workflow) {
  auto branch = dynamic_cast<const WorkflowBranch *>(workflow);
  return branch != nullptr;
}

void WorkflowExecutor::printSuspendingMessage(AbstractWorkflow *workflow) {
  if (!isBranch(workflow)) {
    LOG("** Suspending execution for user interaction: Executing all reachable interaction "
        "nodes, then exiting **");
  }
}

void WorkflowExecutor::printCancelMessage(AbstractWorkflow *workflow) {
  if (!isBranch(workflow)) {
    LOG("Workflow execution successfully cancelled.");
  }
}

void WorkflowExecutor::printExecutionResult(AbstractWorkflow *workflow, const WorkflowExecution *execution) {
  std::string workflowType = isBranch(workflow) ? "Branch" : "Workflow";
  printLogSeparator(true);
  LOG(workflowType << " execution has ended with status: " +
                        executionStateToString(execution->getState()));
}

void WorkflowExecutor::printLogSeparator(bool longSeparator) {
  int num = longSeparator ? config::LOG_SEPARATOR_NUM_LONG : config::LOG_SEPARATOR_NUM_SHORT;
  LOG(std::string(num, config::LOG_SEPARATOR));
}
