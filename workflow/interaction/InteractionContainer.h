/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

template<class T>
class Interaction;
enum class InteractionDirection;
enum class InteractionType;


/**
 * This class encapsulates multiple interactions in one container and also acts like a factory,
 * storing parameters to create new instances when needed.
 * The getter actively creates the instance for given `key` if it does not exist yet.
 * @tparam T type for the interaction
 */
template<class T>
class InteractionContainer {

public:
  InteractionContainer(std::string id, int order, InteractionDirection direction, InteractionType type,
                       std::string description)
      : id(std::move(id)), order(order), direction(direction), type(type), description(std::move(description)) {
  }

  Interaction<T> *getInteraction(int key = 0) {
    if (interactions.find(key) == interactions.end()) {
      interactions[key] = std::make_unique<Interaction<T>>(id, order, direction, type, description);
    }
    return interactions[key].get();
  }

private:
  std::string id;
  int order;
  InteractionDirection direction;
  InteractionType type;
  std::string description;

  std::unordered_map<int, std::unique_ptr<Interaction<T>>> interactions;
};
