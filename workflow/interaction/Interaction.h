/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <nlohmann/json.hpp>
#include <regex>
#include <string>

using json = nlohmann::json;

namespace t {
  struct CropSelection;
  void to_json(json &j, const CropSelection &c);
  void from_json(const json &j, CropSelection &c);
} // namespace t

enum class InteractionDirection {
  INPUT,
  OUTPUT
};

enum class InteractionType {
  FILE,
  STRING,
  INT,
  FLOAT,
  BOOL,
  CROP_IMAGES,
  CHOOSE,
  WEB_VIEW,
  PERIODIC_TABLE
};

/* Define helper functions for the enums outside the template class Interaction, to make them independent
 * of the type paramater */
namespace Interactions {
  static std::string typeToString(InteractionType type) {
    switch (type) {
      case InteractionType::FILE:
        return "file";
      case InteractionType::STRING:
        return "string";
      case InteractionType::INT:
        return "int";
      case InteractionType::FLOAT:
        return "float";
      case InteractionType::BOOL:
        return "bool";
      case InteractionType::CROP_IMAGES:
        return "cropImages";
      case InteractionType::CHOOSE:
        return "choose";
      case InteractionType::WEB_VIEW:
        return "webView";
      case InteractionType::PERIODIC_TABLE:
        return "periodicTable";
    }
    throw std::invalid_argument("Invalid argument: Unsupported interaction type");
  }

  static InteractionType typeFromString(const std::string &string) {
    if (string == "file") {
      return InteractionType::FILE;
    } else if (string == "string") {
      return InteractionType::STRING;
    } else if (string == "int") {
      return InteractionType::INT;
    } else if (string == "float") {
      return InteractionType::FLOAT;
    } else if (string == "bool") {
      return InteractionType::BOOL;
    } else if (string == "cropImages") {
      return InteractionType::CROP_IMAGES;
    } else if (string == "choose") {
      return InteractionType::CHOOSE;
    } else if (string == "webView") {
      return InteractionType::WEB_VIEW;
    } else if (string == "periodicTable") {
      return InteractionType::PERIODIC_TABLE;
    }
    throw std::invalid_argument("Invalid argument: Unknown interaction type: " + (string.empty() ? "(empty string)" : string));
  }

  static InteractionDirection directionFromString(const std::string &string) {
    if (string == "input") {
      return InteractionDirection::INPUT;
    } else if (string == "output") {
      return InteractionDirection::OUTPUT;
    }
    throw std::invalid_argument("Invalid argument: Unknown interaction direction: " + (string.empty() ? "(empty string)" : string));
  }
} // namespace Interactions

template<class T>
class Interaction {
public:
  Interaction(std::string id, int order, InteractionDirection direction, InteractionType type, std::string description)
      : nodeId(std::move(id)), pageNumber(0), order(order), loopIteration(0), direction(direction), type(type),
        description(std::move(description)) {
  }

  json toJson() {
    json jsonObj;
    jsonObj["id"] = createId(nodeId, loopIteration);
    jsonObj["pageNumber"] = pageNumber;
    jsonObj["order"] = order;
    jsonObj["direction"] = direction == InteractionDirection::INPUT ? "input" : "output";
    jsonObj["type"] = Interactions::typeToString(type);
    jsonObj["description"] = description;
    if (default_value) {
      json j = *default_value;
      jsonObj["default_value"] = j;
    }
    if (value) {
      jsonObj["value"] = *value;
    } else {
      jsonObj["value"] = nullptr; // creates "null"
    }
    if (!path.empty()) {
      jsonObj["url"] = path;
    }
    if (!options.empty()) {
      jsonObj["options"] = options;
    }
    if (!url.empty()) {
      jsonObj["url"] = url;
    }
    return jsonObj;
  }

  void fromJson(json jsonObj) {
    auto id = jsonObj["id"].get<std::string>();

    // read the info from the composite id created by "createId()"
    const std::regex compositIdRegex("^(\\{[^}]+\\})_([0-9])+$");
    std::smatch match;
    if (std::regex_match(id, match, compositIdRegex)) {
      nodeId = match[1].str();
      std::string loopIterationsStr = match[2].str();
      loopIteration = (int) std::stol(loopIterationsStr);
    }

    pageNumber = jsonObj["pageNumber"].get<int>();
    order = jsonObj["order"].get<int>();
    if (jsonObj["direction"].get<std::string>() == "input") {
      direction = InteractionDirection::INPUT;
    } else if (jsonObj["direction"].get<std::string>() == "output") {
      direction = InteractionDirection::OUTPUT;
    } else {
      throw std::logic_error("Parsing error: Unknown interaction direction " + jsonObj["direction"].get<std::string>());
    }
    type = Interactions::typeFromString(jsonObj["type"].get<std::string>());
    description = jsonObj["description"].get<std::string>();
    if (jsonObj.contains("default_value")) {
      default_value = std::make_unique<T>(jsonObj["default_value"]);
    }
    if (jsonObj.contains("value") && !jsonObj["value"].is_null()) {
      value = std::make_unique<T>(jsonObj["value"]);
    }
    if (jsonObj.contains("url") && !jsonObj["url"].is_null()) {
      url = jsonObj["url"].get<std::string>();
    }
  }

  bool isCompleted() const {
    return (value != nullptr);
  }

  void setDefaultValue(std::unique_ptr<T> defaultValue) {
    default_value = std::move(defaultValue);
  }

  const std::unique_ptr<T> &getDefaultValue() const {
    return default_value;
  }

  void setValue(std::unique_ptr<T> value) {
    Interaction::value = std::move(value);
  }

  void setPath(const std::string &path) {
    Interaction::path = path;
  }

  void setDescription(const std::string &description) {
    Interaction::description = description;
  }

  T *getValue() const {
    return value.get();
  }

  void setPageNumber(int pageNumber) {
    Interaction::pageNumber = pageNumber;
  }

  void setLoopIteration(int loopIteration) {
    Interaction::loopIteration = loopIteration;
  }

  void setUrl(const std::string &url) {
    Interaction::url = url;
  }

  int getPageNumber() const {
    return pageNumber;
  }

  const std::vector<std::string> &getOptions() const {
    return options;
  }

  void setOptions(const std::vector<std::string> &options) {
    Interaction::options = options;
  }

  static std::string createId(std::string nodeId, int loopIteration) {
    if (loopIteration == 0) {
      return nodeId;
    } else {
      // to distinguish between interactions with the same nodeId but coming from different iterations of the loop
      return nodeId + "_" + std::to_string(loopIteration);
    }
  }

private:
  std::string nodeId;
  int pageNumber;
  int order;
  int loopIteration; // in case iteration gets looped and therefore needs multiple instances in the json
  InteractionDirection direction;
  InteractionType type;
  std::string description;
  std::unique_ptr<T> default_value;
  std::unique_ptr<T> value;
  std::string path;
  std::vector<std::string> options;
  std::string url;
};
