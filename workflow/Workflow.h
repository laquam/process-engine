/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "AbstractWorkflow.h"
#include "nodes/Node.h"
#include <memory>
#include <nlohmann/json.hpp>
#include <unordered_set>
#include <utility>
#include <vector>

class WorkflowExecution;

class Workflow : public AbstractWorkflow {
public:
  explicit Workflow();
  ~Workflow() override = default;

  // interface implementations
  Node *getNode(const std::string &nodeId) const override;
  const Node *getNodeConst(const std::string &nodeId) const override;
  std::unordered_map<std::string, Node *> getNodes() override;
  std::unordered_map<std::string, const Node *> getNodesConst() const override;
  bool isEmpty() const override;
  bool areCoherentGroupsInitialized() const override;

  // specific to this type of AbstractWorkflow
  void addNode(std::unique_ptr<Node> node);
  void connectPorts(std::string const &nodeLeftId, int portLeftIndex, std::string const &nodeRightId, int portRightIndex, bool throwOnFailure = true);

  /**
   * Internally initializes the coherent groups for all SystemCommandNodes.
   * Must be called before generating the execution order (and before executing the workflow).
   * Should be called after all nodes and connections were added, to avoid the need to reinitialize the groups.
   */
  void initCoherentGroups() override;


private:
  std::unordered_map<std::string, std::unique_ptr<Node>> nodes;

  bool coherentGroupsInitialized;
};
