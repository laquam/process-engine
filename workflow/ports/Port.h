/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../StaticData.h"
#include <iostream>
#include <memory>
#include <string>
#include <unordered_set>
#include <utility>
#include <vector>

enum class PortDirection {
  In,
  Out
};

// forward declarations
class ToolNode;
class Workflow;
class WorkflowNodeStates;

class Port {

public:
  Port(int index, PortDirection direction);
  virtual ~Port() = default;
  virtual void addConnectedPort(const Port *connectedPort);
  virtual std::string toString() const;

  const std::string &getNodeId() const;
  void setNodeId(const std::string &nodeId);
  const Node *getNode() const;
  void setNode(const Node *node);
  int getIndex() const;
  const PortDirection &getDirection() const;
  std::vector<const Port *> getConnectedPorts() const;

  std::unordered_set<std::string> getDependencies() const;
  std::unordered_set<std::string> getDependents() const;

  void setWorkflow(Workflow *workflow);

  friend class Node;

protected:
  std::vector<const Port *> connectedPorts;
  std::string nodeId;
  const Node *node; // this back reference is necessary for environment connections
  PortDirection direction;
  int index;
  Workflow *workflow;

private:
  std::unordered_set<std::string> getConnectedNodeIds() const;
};
