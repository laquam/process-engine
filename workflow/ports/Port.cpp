/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "Port.h"
#include "../state/WorkflowNodeStates.h"
#include <algorithm>
#include <unordered_set>

Port::Port(int index, PortDirection direction)
    : index(index), direction(direction) {
}

void Port::addConnectedPort(const Port *connectedPort) {
  if (!connectedPort) {
    throw std::invalid_argument("Can not add null as connected port");
  }
  if (connectedPort->direction == direction) {
    throw std::invalid_argument("Input ports can only be connected to output ports and vice versa");
  }

  this->connectedPorts.push_back(connectedPort);
}

int Port::getIndex() const {
  return index;
}

const PortDirection &Port::getDirection() const {
  return direction;
}

void Port::setNodeId(const std::string &nodeId) {
  this->nodeId = nodeId;
}

const std::string &Port::getNodeId() const {
  return nodeId;
}

std::vector<const Port *> Port::getConnectedPorts() const {
  return connectedPorts;
}

std::unordered_set<std::string> Port::getDependencies() const {
  if (direction == PortDirection::Out) {
    // an output port has no dependencies
    return {};
  } else if (direction == PortDirection::In) {
    return getConnectedNodeIds();
  }
  throw std::logic_error("Unknown port direction");
}

std::unordered_set<std::string> Port::getDependents() const {
  if (direction == PortDirection::In) {
    // an input port has no dependents
    return {};
  } else if (direction == PortDirection::Out) {
    return getConnectedNodeIds();
  }
  throw std::logic_error("Unknown port direction");
}

void Port::setWorkflow(Workflow *w) {
  workflow = w;
}

std::unordered_set<std::string> Port::getConnectedNodeIds() const {
  std::unordered_set<std::string> connectedPortIds;
  for (const Port *otherPort : connectedPorts) {
    if (!otherPort) {
      throw std::logic_error("Unable to get reference to connected port");
    }
    std::string otherNodeId = otherPort->getNodeId();
    connectedPortIds.insert(otherNodeId);
  }
  return connectedPortIds;
}

std::string Port::toString() const {
  // A simple representation to identify the port (node id, direction, index) for debugging/logging
  return getNodeId() + ":" + (getDirection() == PortDirection::Out ? "Out" : "In") + ":" + std::to_string(index);
}

const Node *Port::getNode() const {
  return node;
}

void Port::setNode(const Node *n) {
  node = n;
}
