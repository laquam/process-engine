/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once


#include "Workflow.h"

class WorkflowExecutor {
public:
  static bool execute(AbstractWorkflow *workflow, WorkflowExecution *execution, bool dryRun, bool detach, bool haltForInteraction = true);

private:
  static bool isBranch(const AbstractWorkflow *workflow);

  // printing of log messages
  static void printExecutionStartMessage(const AbstractWorkflow *workflow, const WorkflowExecution *execution,
                                         bool dryRun);
  static void
  printNodeExecutionHeader(const AbstractWorkflow *workflow, const WorkflowExecution *execution, const Node *node);
  static void printSuspendingMessage(AbstractWorkflow *workflow);
  static void printExecutionResult(AbstractWorkflow *workflow, const WorkflowExecution *execution);
  static void printLogSeparator(bool longSeparator = false);
  static void printCancelMessage(AbstractWorkflow *workflow);
};

// used for waiting for signal SIGUSR1
static sigset_t mysigset;
static sigset_t myoldsigset;
