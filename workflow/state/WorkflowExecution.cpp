/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "WorkflowExecution.h"
#include "../../Logger.h"
#include "../Workflow.h"
#include "../WorkflowBranch.h"
#include "../nodes/SystemCommandNode.h"
#include "WorkflowNodeStates.h"
#include <fstream>
#include <iomanip>
#include <stdexcept>

WorkflowExecution::WorkflowExecution(const AbstractWorkflow *workflow, bool enablePersistence,
                                     WorkflowExecution *parent)
    : workflow(workflow), enablePersistence(enablePersistence), parentExecution(parent),
      nodeStates(std::make_unique<WorkflowNodeStates>(workflow)), pid(0), iteration(0),
      loopIteration(0), state(ExecutionState::READY), cancelExecution(false) {
  if (parent) {
    iteration = parent->getIteration() + 1;
  }
}


json WorkflowExecution::toJson() const {
  json jsonObj;
  jsonObj["pid"] = pid;
  jsonObj["fileName"] = fileName;
  jsonObj["state"] = executionStateToString(state);
  jsonObj["startDateTime"] = startDateTime;
  jsonObj["endDateTime"] = endDateTime;
  jsonObj["processEngine"] = "SequentialPE";
  jsonObj["nodesProcessed"] = getNodesProcessed();
  jsonObj["nodesTotal"] = getNodesToProcess();
  jsonObj["iteration"] = iteration;

  jsonObj[config::NODE_STATES_KEY] = nodeStates->toJson();
  jsonObj[config::VARIABLES_KEY] = variables;

  return jsonObj;
}

void WorkflowExecution::setState(ExecutionState newState) {
  state = newState;
}

void WorkflowExecution::setFileName(const std::string &s) {
  fileName = s;
}

void WorkflowExecution::setStartDateTime(const std::string &dateTime) {
  startDateTime = dateTime;
}

void WorkflowExecution::persistInfo() {
  if (enablePersistence) {
    json jsonInfo = toJson();
    std::ofstream out(config::INFO_FILENAME);
    out << std::setw(4) << jsonInfo << std::endl;
  }
}

void WorkflowExecution::setPid(pid_t p) {
  pid = p;
}

int WorkflowExecution::getIteration() const {
  if (parentExecution) {
    return parentExecution->getIteration();
  }
  return iteration;
}

void WorkflowExecution::setEndDateTime(const std::string &dateTime) {
  endDateTime = dateTime;
}


ExecutionState WorkflowExecution::getState() const {
  return state;
}

void WorkflowExecution::setIteration(int n) {
  iteration = n;
}


void WorkflowExecution::restoreNodeStates(const json &obj) {
  nodeStates->fromJson(obj);
}

int WorkflowExecution::getNodesProcessed() const {
  return nodeStates->getNodesProcessed();
}

WorkflowNodeStates *WorkflowExecution::getNodeStates() const {
  return nodeStates.get();
}

int WorkflowExecution::getNodesToProcess() const {
  return nodeStates->getNodesToProcess();
}

bool WorkflowExecution::nodeStillToProcess(const std::string &nodeId) {
  bool result = nodeStates->getNodeState(nodeId).stillToProcess();

  // in case this node is in a coherent group and one of it's members must be processed, we have to find out
  auto systemCommandNode = dynamic_cast<const SystemCommandNode *>(workflow->getNode(nodeId));
  if (systemCommandNode) {
    if (systemCommandNode->isInCoherentGroup()) {
      bool groupNeedsExecution = false;
      CoherentNodeGroup *coherentGroup = systemCommandNode->getCoherentGroup();
      for (const auto &coherentNode : coherentGroup->getCoherentNodes()) {
        if (nodeStates->getNodeState(coherentNode.connectedNode->getId()).stillToProcess()) {
          groupNeedsExecution = true;
          break;
        }
      }
      result |= groupNeedsExecution;
    }
  }

  return result;
}

bool WorkflowExecution::nodeNeedsPostExecution(const std::string &nodeId) {
  return nodeStates->getNodeState(nodeId).needsPostExecution();
}

bool WorkflowExecution::nodeNeedsInteraction(const std::string &nodeId) {
  return nodeStates->getNodeState(nodeId).needsInteraction();
}

WorkflowExecution *WorkflowExecution::registerBranch(const WorkflowBranch *branch) {
  auto subExecution = std::make_unique<WorkflowExecution>(branch, false, this);
  nodeStates->markBranchNodes(branch);
  const std::string &rootNodeId = branch->getIdentifier();
  branchExecutions[rootNodeId] = std::move(subExecution);
  return branchExecutions[rootNodeId].get();
}

void WorkflowExecution::skipBranch(const std::string &branchId) {
  if (branchExecutions.find(branchId) == branchExecutions.end()) {
    throw std::invalid_argument(
      "Branch for identifier " + branchId + " was not registered");
  }
  branchExecutions[branchId]->markToSkip(); // 1. mark nodes in branch to be skipped
  branchExecutionFinished(branchId);        // 2. join the node states back in the main execution
                                            //    (as if it's execution was finished)
}

void WorkflowExecution::markToSkip() {
  nodeStates->markToSkip();
}

WorkflowExecution *WorkflowExecution::getBranchExecution(const std::string &branchId) const {
  if (branchExecutions.find(branchId) == branchExecutions.end()) {
    throw std::invalid_argument(
      "Branch for identifier " + branchId + " was not registered");
  }
  return branchExecutions.at(branchId).get();
}

void WorkflowExecution::branchExecutionFinished(const std::string &branchId) {
  if (branchExecutions.find(branchId) == branchExecutions.end()) {
    throw std::invalid_argument(
      "Branch for identifier " + branchId + " was not registered");
  }
  auto branchExecution = branchExecutions[branchId].get();

  // join in the node states that resulted from the branch execution
  *nodeStates += *branchExecution->getNodeStates();
}

void WorkflowExecution::updateNodeStatesFromParent() {
  if (!parentExecution) {
    throw std::logic_error("Can not update from parent: No valid parent reference available");
  }
  nodeStates->updateBranchFrom(parentExecution->getNodeStates());
}

void WorkflowExecution::setVariable(const std::string &name, const std::string &value, bool overwrite) {
  if (parentExecution) {
    // note: this would be a problem when running branches in parallel because they all share the same variable store
    parentExecution->setVariable(name, value, overwrite);
  } else {
    if (std::regex_search(value, config::VARIABLE_REGEX)) {
      throw std::logic_error("Variables are not allowed to contain other Variables");
    }
    if (overwrite || (variables.find(name) == variables.end())) {
      variables[name] = std::string(value);
    } else {
      LOG_VERBOSE("Variable \""
                  << "\" was not changed (already set)");
    }
  }
}

std::string WorkflowExecution::expandVariables(const std::string &string) const {
  if (parentExecution) {
    return parentExecution->expandVariables(string);
  }
  std::string result(string);
  std::smatch matches;

  int i = 0;
  while (std::regex_search(result, matches, config::VARIABLE_REGEX)) {
    if (++i > string.size()) throw std::logic_error("Maximum variable replacement attempts exceeded.");
    std::string to_replace(matches[0]);
    std::string variable_name(matches[1]);

    auto variable = variables.find(variable_name);
    if (variable != variables.end()) {
      LOG_VERBOSE("Replacing variable " << variable_name << " with value \"" << variable->second << "\".");
      result.replace(matches.position(0), to_replace.size(), variable->second);
    } else {
      LOG("Warning: Variable " << variable_name << " not set!");
      result.replace(matches.position(0), to_replace.size(), config::VARIABLE_VALUE_UNDEFINED);
    }
  }
  return result;
}

void WorkflowExecution::restoreVariables(const json &obj) {
  variables = obj.get<std::unordered_map<std::string, std::string>>();
}

void WorkflowExecution::setNodeExecutionState(const std::string &nodeId, NodeExecutionState newState) {
  nodeStates->updateNodeExecutionState(nodeId, newState);
}

NodeState WorkflowExecution::getNodeState(const std::string &nodeId) const {
  return nodeStates->getNodeState(nodeId);
}

int WorkflowExecution::getLoopIteration() const {
  return loopIteration;
}

void WorkflowExecution::setLoopIteration(int newLoopIteration) {
  loopIteration = newLoopIteration;
}

pid_t WorkflowExecution::getPid() const {
  return pid;
}

void WorkflowExecution::cancel() {
  if (parentExecution) {
    parentExecution->cancel();
    return;
  }
  cancelExecution = true;
  setState(ExecutionState::CANCELLING);
  persistInfo();
}

bool WorkflowExecution::isCancelling() {
  if (parentExecution) {
    return parentExecution->isCancelling();
  }
  return cancelExecution;
}
