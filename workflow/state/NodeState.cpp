/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "NodeState.h"

json NodeState::toJson() const {
  if (!customInfo.empty()) {
    return json::array({currentState, successfulExecutions, customInfo});
  }
  return json::array({currentState, successfulExecutions});
}

void NodeState::fromJson(const json &obj) {
  currentState = obj[0];
  successfulExecutions = obj[1];
  if (obj.size() > 2) {
    customInfo = obj[2];
  }
}

bool NodeState::dependencyResolved() const {
  return (currentState == NodeExecutionState::EXECUTED) || (currentState == NodeExecutionState::NOOP);
}

bool NodeState::stillToProcess() const {
  return (currentState == NodeExecutionState::TO_BE_EXECUTED) || (currentState == NodeExecutionState::NEEDS_INTERACTION);
}

bool NodeState::persist() const {
  return currentState != NodeExecutionState::NOOP;
}

bool NodeState::needsInteraction() const {
  return currentState == NodeExecutionState::NEEDS_INTERACTION;
}

NodeState NodeState::operator+(const NodeState &other) {
  NodeState result = *this;
  if (other.successfulExecutions < 0) {
    throw std::invalid_argument("NodeStates are not allowed to have negative execution count");
  }
  result.currentState = other.currentState;
  result.successfulExecutions = other.successfulExecutions;
  result.customInfo = other.customInfo;
  return result;
}

bool NodeState::countProcessed() const {
  return currentState == NodeExecutionState::EXECUTED;
}

bool NodeState::countTotal() const {
  return (currentState != NodeExecutionState::NOOP) && (currentState != NodeExecutionState::NOT_SET) && (currentState != NodeExecutionState::TO_BE_SKIPPED);
}

NodeState NodeState::convertSkip() const {
  NodeExecutionState newState = currentState;
  if (currentState != NodeExecutionState::NOOP) {
    newState = NodeExecutionState::TO_BE_SKIPPED;
  }
  return NodeState{newState, successfulExecutions, customInfo};
}

NodeState NodeState::updateFromParent(NodeState parentNodeState) const {
  NodeState result = *this; // copy intended
  if (parentNodeState.currentState == NodeExecutionState::EXECUTE_IN_BRANCH) {
    return result;
  } else {
    return NodeState{parentNodeState.currentState, successfulExecutions, customInfo};
  }
}

bool NodeState::needsPostExecution() const {
  return currentState == NodeExecutionState::EXECUTED;
}

int NodeState::getSuccessfulExecutions() const {
  return successfulExecutions;
}
