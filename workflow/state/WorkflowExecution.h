/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once


#include "ExecutionState.h"
#include "WorkflowNodeStates.h"
#include <csignal>
#include <nlohmann/json.hpp>
#include <string>
#include <unordered_set>

using json = nlohmann::json;

class AbstractWorkflow;
class WorkflowBranch;
class WorkflowNodeStates;

class WorkflowExecution {
public:
  explicit WorkflowExecution(const AbstractWorkflow *workflow = nullptr, bool enablePersistence = false, WorkflowExecution *parent = nullptr);

  WorkflowExecution *registerBranch(const WorkflowBranch *branch);

  /**
   * To be called after a branch execution finished.
   * Will join back in the WorkflowExecution for the branch, taking care of the NodeStates.
   * @param branchId Identifier of the branch
   */
  void branchExecutionFinished(const std::string &branchId);

  /**
   * Mark a branch (all it's nodes) to be skipped in total. This is necessary for the IfNode, for example.
   * It updates the NodeStates and joins them back to the parent execution.
   * @param branchId Identifier of the branch
   */
  void skipBranch(const std::string &branchId);

  WorkflowExecution *getBranchExecution(const std::string &branchId) const;

  void setVariable(const std::string &name, const std::string &value, bool overwrite = true);
  std::string expandVariables(const std::string &string) const;

  void setPid(pid_t pid);
  void setState(ExecutionState state);
  void setFileName(const std::string &fileName);
  void setStartDateTime(const std::string &dateTime);
  void setEndDateTime(const std::string &dateTime);
  void setIteration(int iteration);
  void setLoopIteration(int newLoopIteration);

  // interface methods related to nodeState and statistics
  pid_t getPid() const;
  ExecutionState getState() const;
  WorkflowNodeStates *getNodeStates() const;
  NodeState getNodeState(const std::string &nodeId) const;
  void setNodeExecutionState(const std::string &nodeId, NodeExecutionState newState);
  bool nodeStillToProcess(const std::string &nodeId);
  bool nodeNeedsPostExecution(const std::string &nodeId);
  bool nodeNeedsInteraction(const std::string &nodeId);
  int getNodesToProcess() const;
  int getNodesProcessed() const;
  int getIteration() const;
  int getLoopIteration() const;

  // persistence
  void restoreNodeStates(const json &obj);
  void restoreVariables(const json &obj);
  json toJson() const;
  void persistInfo();

  void updateNodeStatesFromParent();

  /**
   * When cancel() was called, the Executor should attempt to gracefully shutdown the process of executing the workflow.
   * This decision can not be undone!
   */
  void cancel();

  /**
   * @return true if execution is about to be cancelled
   */
  bool isCancelling();

private:
  // Mark this execution (of Workflow/Branch) to be skipped entirely.
  void markToSkip();

  const AbstractWorkflow *workflow;
  bool enablePersistence;
  WorkflowExecution *parentExecution;
  std::unique_ptr<WorkflowNodeStates> nodeStates; // holds state of each node for this specific execution
  std::unordered_map<std::string, std::string> variables;

  // sub executions of branches (allow one per Node)
  std::unordered_map<std::string, std::unique_ptr<WorkflowExecution>> branchExecutions;

  // pid of the running execution
  pid_t pid;

  /* number which will be increased by one every time a specific workflow will be executed/continued
   * (used to group interactions into "pages") */
  int iteration;

  // separate iteration counters for in case this workflow will be executed in a loop
  // (used to distinguish between multiple interactions with the same nodeId)
  int loopIteration;

  ExecutionState state;
  std::string fileName;
  std::string startDateTime;
  std::string endDateTime;
  std::string infoFileName;

  // this will be true if a SIGTERM was received, and a graceful shutdown should be performed
  bool cancelExecution;
};
