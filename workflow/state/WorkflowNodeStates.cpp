/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "WorkflowNodeStates.h"
#include "../Workflow.h"
#include "../WorkflowBranch.h"

WorkflowNodeStates::WorkflowNodeStates(const AbstractWorkflow *workflow)
    : workflow(workflow) {
  reset();
}

NodeState WorkflowNodeStates::getNodeState(const std::string &nodeId) const {
  if (nodeStates.find(nodeId) == nodeStates.end()) {
    throw std::invalid_argument("Internal error: Node state for " + nodeId + " not available");
  }
  return nodeStates.at(nodeId);
}

void WorkflowNodeStates::setNodeState(const std::string &nodeId, NodeState newState) {
  nodeStates[nodeId] = newState;
}

json WorkflowNodeStates::toJson() const {
  json result = json::object();
  for (const auto &nodeState : nodeStates) {
    if (nodeState.second.persist())
      result.push_back({nodeState.first, nodeState.second.toJson()});
  }
  return result;
}

void WorkflowNodeStates::reset(bool keepCounters) {
  if (workflow) {
    for (const auto &node : workflow->getNodesConst()) {
      NodeState result = node.second->getInitialState();
      if (keepCounters && (nodeStates.find(node.first) != nodeStates.end())) {
        result.successfulExecutions = nodeStates.at(node.first).successfulExecutions;
      }
      nodeStates[node.first] = result;
    }
  }
}

void WorkflowNodeStates::fromJson(const json &obj) {
  reset();
  for (auto it = obj.begin(); it != obj.end(); ++it) {
    const auto &nodeId = it.key();
    NodeState parsedState{};
    parsedState.fromJson(it.value());
    setNodeState(nodeId, parsedState);
  }
}

int WorkflowNodeStates::getNodesToProcess() const {
  int nodesToProcess = 0;
  for (const auto &nodeState : nodeStates) {
    if (nodeState.second.countTotal()) {
      nodesToProcess++;
    }
  }
  return nodesToProcess;
}

int WorkflowNodeStates::getNodesProcessed() const {
  int nodesProcessed = 0;
  for (const auto &nodeState : nodeStates) {
    if (nodeState.second.countProcessed()) {
      nodesProcessed++;
    }
  }
  return nodesProcessed;
}

void WorkflowNodeStates::markBranchNodes(const WorkflowBranch *branch) {

  // set the state in the parent WorkflowNodeStates to EXECUTE_IN_BRANCH,
  // signaling that they must be executed in the branch otherwise they will be executed on top level also
  for (const auto &nodeId : branch->getNodeIds()) {
    NodeState previousNodeState = getNodeState(nodeId);
    // warning: ignoring the previous execution state here
    if (previousNodeState.stillToProcess()) {
      updateNodeExecutionState(nodeId, NodeExecutionState::EXECUTE_IN_BRANCH);
    }
  }
}

WorkflowNodeStates &WorkflowNodeStates::operator+=(const WorkflowNodeStates &rhs) {
  // note: intentionally ignoring rhs's workflow back-ref

  for (const auto &nodeState : rhs.nodeStates) {
    if (nodeStates.find(nodeState.first) != nodeStates.end()) {
      NodeState state = nodeStates[nodeState.first];
      setNodeState(nodeState.first, state + nodeState.second); // takes care of counting processedStates
    } else {
      throw std::invalid_argument("Error joining in WorkflowNodeStates of a branch: Branch contains unknown node");
    }
  }
  return *this;
}

void WorkflowNodeStates::markToSkip() {
  for (const auto &item : nodeStates) {
    const std::string &nodeId = item.first;
    NodeState previousNodeState = getNodeState(nodeId);
    setNodeState(nodeId, previousNodeState.convertSkip());
  }
}

void WorkflowNodeStates::updateBranchFrom(const WorkflowNodeStates *parentNodeStates) {
  for (const auto &item : nodeStates) {
    const std::string &nodeId = item.first;
    NodeState parentNodeState = parentNodeStates->getNodeState(nodeId);
    NodeState previousNodeState = getNodeState(nodeId);
    setNodeState(nodeId, previousNodeState.updateFromParent(parentNodeState));
  }
}

void WorkflowNodeStates::updateNodeExecutionState(const std::string &nodeId, NodeExecutionState newState) {
  NodeState state = getNodeState(nodeId);
  if (newState == NodeExecutionState::EXECUTED) {
    state.successfulExecutions++;
  }
  state.currentState = newState;
  setNodeState(nodeId, state);
}
