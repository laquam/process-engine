/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <nlohmann/json.hpp>

using json = nlohmann::json;

enum class NodeExecutionState {
  NOT_SET,
  NOOP, // no execution necessary, do not count
  TO_BE_EXECUTED,
  TO_BE_SKIPPED,
  EXECUTED,
  EXECUTE_IN_BRANCH, // do not execute, will be executed in a branch
  NEEDS_INTERACTION,
  ERROR
};

NLOHMANN_JSON_SERIALIZE_ENUM(NodeExecutionState, {
                                                   {NodeExecutionState::NOT_SET, nullptr},
                                                   {NodeExecutionState::NOOP, "N"},
                                                   {NodeExecutionState::TO_BE_EXECUTED, "TBE"},
                                                   {NodeExecutionState::TO_BE_SKIPPED, "TBS"},
                                                   {NodeExecutionState::EXECUTED, "EX"},
                                                   {NodeExecutionState::EXECUTE_IN_BRANCH, "BR"},
                                                   {NodeExecutionState::NEEDS_INTERACTION, "NI"},
                                                   {NodeExecutionState::ERROR, "ER"},
                                                 })

class NodeState {
public:
  NodeExecutionState currentState = NodeExecutionState::NOT_SET;
  int successfulExecutions = 0;
  json customInfo;

  NodeState operator+(NodeState const &rhs);

  int getSuccessfulExecutions() const;

  json toJson() const;
  void fromJson(const json &obj);

  /**
   * This functions determines if the state of a node allows
   * dependent nodes to be executed.
   * @return True if the execution state allows dependent nodes to be executed.
   */
  bool dependencyResolved() const;

  /**
   * Returns true if the node must still be processed.
   * Used for statistics only.
   * @return True if the node still must be processed according to this node state
   */
  bool stillToProcess() const;

  /**
   * Returns true if the node needs to execute it's post execution hook.
   * @return True if post execution hook should be run
   */
  bool needsPostExecution() const;

  /**
   * Decides whether this node state must to be persisted between workflow executions.
   * @return True if the node state information is important after execution.
   */
  bool persist() const;

  /**
   * Decides whether this node state implies that the node needs user interaction.
   * @return True if the node needs interaction.
   */
  bool needsInteraction() const;

  /**
   * Decides whether a node with this NodeState should be counted to the number of processed nodes.
   * @return True if the node with this state should be counted to processed nodes
   */
  bool countProcessed() const;

  /**
   * Decides whether a node with this NodeState should be counted to the total number of nodes in the workflow.
   * @return True if the node with this state should be counted to nodes total
   */
  bool countTotal() const;

  /**
   * Get the according node state for a node having this current node state and which must be skipped.
   * @return A new NodeState
   */
  NodeState convertSkip() const;

  /**
   * If a node in a branch needs to be executed, it's state in the parent execution will be EXECUTE_IN_BRANCH.
   * But it could change in the parent, for example to TO_BE_SKIPPED.
   * This function computes the result to update from the parent if necessary.
   */
  NodeState updateFromParent(NodeState parentNodeState) const;
};
