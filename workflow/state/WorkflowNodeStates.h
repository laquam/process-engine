/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once


#include "../AbstractWorkflow.h"
#include "NodeState.h"
#include <nlohmann/json.hpp>
#include <string>
#include <unordered_map>

using json = nlohmann::json;

class Workflow;
class AbstractWorkflow;
class WorkflowBranch;

class WorkflowNodeStates {

public:
  explicit WorkflowNodeStates(const AbstractWorkflow *workflow = nullptr);
  void reset(bool keepCounters = false);

  /**
   * This operator is used to join in WorkflowNodeState objects of branches of `workflow`,
   * when their execution finishes, to sum up NodeStates of the whole workflow (including branch executions).
   */
  WorkflowNodeStates &operator+=(WorkflowNodeStates const &rhs);

  NodeState getNodeState(const std::string &nodeId) const;
  void setNodeState(const std::string &nodeId, NodeState newState);

  /**
   * Function to report change of a nodes' NodeExecutionState during or after node execution.
   * This updates the NodeExecutionState in the stored NodeState, and also increases the number of
   * successful executions if @param newState is EXECUTED.
   * @param nodeId Id of the target node
   * @param newState The new NodeExecutionState.
   */
  void updateNodeExecutionState(const std::string &nodeId, NodeExecutionState newState);

  json toJson() const;
  void fromJson(const json &obj);

  int getNodesToProcess() const;
  int getNodesProcessed() const;

  /**
   * Set the state of all nodes in the branch to EXECUTE_IN_BRANCH,
   * signaling that they must be executed in the branch.
   * @param branch Pointer to WorkflowBranch to get nodeIds from.
   */
  void markBranchNodes(const WorkflowBranch *branch);

  /**
   * Set the state of all nodes to TO_BE_SKIPPED,
   * signaling that they must be skipped.
   * (The effective resulting state is determined according to previous state).
   */
  void markToSkip();

  void updateBranchFrom(const WorkflowNodeStates *parentNodeStates);

private:
  const AbstractWorkflow *workflow;
  std::unordered_map<std::string, NodeState> nodeStates;
};
