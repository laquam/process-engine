/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../Logger.h"
#include "Workflow.h"
#include "nodes/FileOutputNode.h"
#include <iostream>
#include <set>

#include "CoherentNodeGroup.h"
#include "WorkflowAnalyzer.h"
#include "WorkflowBranch.h"
#include "ports/BranchingPort.h"
#include "ports/CoherencyPort.h"
#include "ports/PipePort.h"
#include "state/WorkflowExecution.h"


WorkflowAnalyzer::WorkflowAnalyzer(AbstractWorkflow *workflow) : workflow(workflow) {
  if (!workflow) {
    throw std::invalid_argument("Workflow must be non-null!");
  }
  _nodes = workflow->getNodesConst();
}

std::vector<std::unordered_set<std::string>> WorkflowAnalyzer::generateSequentialExecutionSets() const {
  if (!workflow) {
    throw std::invalid_argument("Workflow must be non-null!");
  }
  if (!workflow->areCoherentGroupsInitialized()) {
    throw std::invalid_argument("Coherent nodes must be initialized before analysis!");
  }

  std::vector<std::unordered_set<std::string>> result;
  std::set<std::string> nodesToProcess;
  std::set<std::string> dependencyFreeNodes;
  WorkflowNodeStates temporaryWorkflowState(workflow);

  for (auto &element : _nodes) {
    auto &nodeId = element.first;
    auto &nodePtr = element.second;

    // initialize pipe connections if not already set


    nodesToProcess.insert(nodeId);
    if (countUnresolvedDependencies(&temporaryWorkflowState, nodeId) == 0) {
      dependencyFreeNodes.insert(nodeId);
    }
  }

  int round = 0;
  while (!nodesToProcess.empty()) {
    // 1. Add all nodes without dependency to a set and add it to the result list
    // if the workflow is valid, the set should never be empty
    std::unordered_set<std::string> currentSet;
    LOG_VERBOSE("Dependency scan round " << round++ << ", free nodes: " << dependencyFreeNodes.size());
    if (round > config::MAX_DEPENDENCY_SCAN_ROUNDS) {
      throw std::logic_error("Maximum execution order scan depth exceeded.");
    }
    if (dependencyFreeNodes.empty()) {
      throw std::logic_error("Unable to find sequential execution order: Could not resolve graph dependencies.");
    }
    for (const auto &nodeId : dependencyFreeNodes) {
      currentSet.insert(nodeId);
    }
    result.push_back(currentSet);

    // 2. Mark all nodes in the set "executed" to remove them as dependency from other nodes
    // also remove them from nodesToProcess and dependencyFreeNodes
    for (const auto &nextNodeId : currentSet) {
      nodesToProcess.erase(nextNodeId);
      dependencyFreeNodes.erase(nextNodeId);
      if (_nodes.find(nextNodeId) != _nodes.end()) {
        auto &nodePtr = _nodes.at(nextNodeId);
        temporaryWorkflowState.setNodeState(nextNodeId, NodeState{
                                                          NodeExecutionState::EXECUTED, 0}); // number of successful executions is irrelevant to the algorithm
      } else {
        throw std::logic_error("Critical: Node with registered id not found");
      }
    }

    // 3. Now there should be new nodes with 0 dependencies, find them and add them to dependencyFreeNodes
    for (const auto &nodeId : nodesToProcess) {
      if (_nodes.find(nodeId) != _nodes.end()) {
        auto &nodePtr = _nodes.at(nodeId);

        if (countUnresolvedDependencies(&temporaryWorkflowState, nodeId) == 0) {
          dependencyFreeNodes.insert(nodeId);
        }
      } else {
        throw std::logic_error("Critical: Node with registered id not found");
      }
    }
  }

  return result;
}

std::shared_ptr<CoherentNodeGroup> WorkflowAnalyzer::findCoherentNodeGroup(const std::string &nodeId) const {

  auto result = std::make_shared<CoherentNodeGroup>();
  const std::string invalidTypeErrorMessage = "Invalid node type connected via coherent connection";
  const std::string unsupportedTopologyErrorMessage = "Erroneous workflow topology:\n"
                                                      "Only one outgoing coherent connection is allowed per node!\n"
                                                      "- Do not connect both ENV and STDOUT ports on the same node\n"
                                                      "- Do not connect ENV or STDOUT ports to more than one node";

  auto nodes = workflow->getNodesConst();
  std::vector<CoherentConnectionInfo> tailConnections;
  std::vector<CoherentConnectionInfo> headConnections;
  std::string currentNodeId = nodeId;
  std::string lastGroupNodeId;

  // 1. go to the beginning of the chain of piped nodes
  do {
    headConnections = nodes[currentNodeId]->getCoherentConnections(PortDirection::In);
    if (headConnections.size() > 1) throw std::logic_error(unsupportedTopologyErrorMessage);
    if (headConnections.size() == 1) {
      CoherentConnectionType connectionType = headConnections[0].connectionType;
      if (connectionType == CoherentConnectionType::INVALID) {
        throw std::logic_error("Invalid coherent connection type");
      }
      if (connectionType == CoherentConnectionType::PIPE_TO_OTHER) {
        // for now only follow "pure" connections to the left, pure means ENV-ENV or PIPE-PIPE
        break;
      }
      // continue to traverse to the left
      currentNodeId = headConnections[0].connectedNodeId;
    }
  } while (!headConnections.empty());

  // 2. add the start node
  auto startNode = dynamic_cast<const SystemCommandNode *>(nodes[currentNodeId]);
  if (startNode) {
    result->addStartNode(startNode);
  } else {
    throw std::logic_error(invalidTypeErrorMessage);
  }

  // 3. go to the end of the chain, add each found node to the result
  do {
    tailConnections = nodes[currentNodeId]->getCoherentConnections(PortDirection::Out);
    if (tailConnections.size() > 1) throw std::logic_error(unsupportedTopologyErrorMessage);
    if (tailConnections.size() == 1) {
      lastGroupNodeId = currentNodeId;
      currentNodeId = tailConnections[0].connectedNodeId;
      /* possible check: is this node in the next execution set? (but no real need to check because
       * an error will be thrown later if the node will be processed but has unresolved dependencies */
      auto followingNode = dynamic_cast<const SystemCommandNode *>(nodes[currentNodeId]);

      if (followingNode && (tailConnections[0].connectionType == CoherentConnectionType::PIPE)) {
        result->addCoherentNode(followingNode, tailConnections[0].connectionType);
      } else {
        /* if the next Node is no SystemCommandNode, or the ports of the connection are not both of type pipe or
         * environment, the group ends here */
        break;
      }
    }
  } while (!tailConnections.empty());

  // Find the stdout output port of the group , so that other nodes can obtain it from the connection.
  if (nodes[lastGroupNodeId]) {
    std::vector<PipePort *> lastNodesOutgoingPipePorts = nodes[lastGroupNodeId]->getPortsByType<PipePort>(
      PortDirection::Out);
    if (lastNodesOutgoingPipePorts.size() > 1) {
      LOG("Warning: The last node in the node group has multiple pipe outputs. Using the first one for stdout.");
    }
    if (!lastNodesOutgoingPipePorts.empty()) {
      result->setPipeOutputPort(lastNodesOutgoingPipePorts[0]);
    }
  }
  if (result->size() > 1) {
    LOG_VERBOSE("\tfound coherent group:\n"
                << result->toString());
    return std::move(result);
  } else {
    return {};
  }
}

int WorkflowAnalyzer::countUnresolvedDependencies(const WorkflowNodeStates *nodeStates, const std::string &nodeId) const {
  if (!workflow->areCoherentGroupsInitialized()) {
    throw std::invalid_argument("Coherent nodes must be initialized before analysis!");
  }
  int result = 0;
  auto node = workflow->getNodeConst(nodeId);
  auto allDependencies = node->getAllDependencies();
  for (const auto &dependency : allDependencies) {
    auto item = _nodes.find(dependency);
    if (item != _nodes.end()) {
      const auto &nodePtr = item->second;
      bool dependencyResolved =
        (nodeStates->getNodeState(item->first).dependencyResolved());
      LOG_VERBOSE("Dependency " << item->first << " is " << (dependencyResolved ? "" : "un") << "resolved");
      if (!dependencyResolved) {
        result++;
      }
    }
  }
  return result;
}

std::unique_ptr<WorkflowBranch>
WorkflowAnalyzer::findBranch(AbstractWorkflow *parent, const std::string &startNodeId, int startPortIndex,
                             const std::function<bool(const Node *)> &exitCondition) {
  const Node *node = _nodes.at(startNodeId);
  if (!node) {
    throw std::invalid_argument("Node does not exist in the workflow");
  }
  const Port *port = node->getPort(PortDirection::Out, startPortIndex);
  if (!port) {
    throw std::invalid_argument("Target node does not have an outgoing port on index " + std::to_string(startPortIndex));
  }
  auto rootPort = dynamic_cast<const BranchingPort *>(port);
  if (!rootPort) {
    throw std::invalid_argument("Can only create branches originating from a port of type BranchingPort");
  }

  auto relevantDependents = rootPort->getDependents();
  std::unordered_set<std::string> result(relevantDependents); // copy
  for (const std::string &explicitDependentId : relevantDependents) {
    std::unordered_set<std::string> branchNodes = findBranchRecursively(explicitDependentId, exitCondition);
    result.insert(branchNodes.begin(), branchNodes.end());
  }

  auto branch = std::make_unique<WorkflowBranch>(parent, rootPort);
  for (const auto &resultNodeId : result) {
    branch->addNode(resultNodeId);
  }
  return std::move(branch);
}

std::unordered_set<std::string> WorkflowAnalyzer::findBranchRecursively(const std::string &nodeId,
                                                                        const std::function<bool(const Node *)> &exitCondition,
                                                                        unsigned int currentRecursionDepth) {
  if (currentRecursionDepth > config::MAX_BRANCH_RECURSION_DEPTH) {
    throw std::logic_error("Maximum recursion depth exceeded while finding workflow branches.");
  }
  const Node *node = _nodes.at(nodeId);
  if (!node) {
    throw std::invalid_argument("Node does not exist");
  }

  if (exitCondition(node)) {
    // exit recursion
    return {};
  }

  // 1. add this node, it is part of the branch
  std::unordered_set<std::string> result = {nodeId};

  // 2. recursively follow all explicit dependencies (means: connected via outgoing dependency connection)
  auto explicitDependents = node->getExplicitDependents(); // explicit dependency on any port
  for (const auto &explicitDependentId : explicitDependents) {
    auto branch = findBranchRecursively(explicitDependentId, exitCondition, currentRecursionDepth + 1);
    result.insert(branch.begin(), branch.end());
  }

  return result;
}
