
#include "../../workflow/Workflow.h"

#include "UserOutputWebViewNode.h"

UserOutputWebViewNode::UserOutputWebViewNode(const std::string &id, const std::string &description,
                                             const std::string &url, int interactionOrder)
    : UserInteractionNode(id), name("UserOutputWebViewNode") {
  // input ports
  promptMessagePort = createPort<DataPort>(1, PortDirection::In);
  urlPort = createPort<DataPort>(2, PortDirection::In);

  // interaction
  interactions = std::make_unique<InteractionContainer<std::string>>(getId(), interactionOrder,
                                                                     InteractionDirection::OUTPUT, InteractionType::WEB_VIEW, description);
}

const std::string &UserOutputWebViewNode::getName() const {
  return name;
}

json UserOutputWebViewNode::getInteractionJson(int loopIteration) const {
  return interactions->getInteraction(loopIteration)->toJson();
}

bool UserOutputWebViewNode::needsInteraction(int loopIteration, int executionIteration) const {
  return executionIteration <= interactions->getInteraction(loopIteration)->getPageNumber();
}

void UserOutputWebViewNode::setInteractionJson(int loopIteration, json interactionJson) const {
  interactions->getInteraction(loopIteration)->fromJson(interactionJson);
}

bool UserOutputWebViewNode::processInputPortData(WorkflowExecution *execution) {
  int loopIteration = execution->getLoopIteration();
  if (promptMessagePort->getData()) {
    auto promptMessage = execution->expandVariables(promptMessagePort->getData()->toString());
    interactions->getInteraction(loopIteration)->setDescription(promptMessage);
  }
  if (urlPort->getData()) {
    auto url = execution->expandVariables(urlPort->getData()->toString());
    interactions->getInteraction(loopIteration)->setUrl(url);
  }
  return true; // no validation of required ports for now
}

bool UserOutputWebViewNode::processOutputPortData(WorkflowExecution *) {
  return true;
}

void UserOutputWebViewNode::initInteraction(int pageNumber, int loopIteration) const {
  interactions->getInteraction(loopIteration)->setPageNumber(pageNumber);
  interactions->getInteraction(loopIteration)->setLoopIteration(loopIteration);
}
