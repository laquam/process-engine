/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "UserInputIntegerNode.h"
#include "../../../Logger.h"
#include "../../ports/DataPort.h"
#include "../../workflow/Workflow.h"

UserInputIntegerNode::UserInputIntegerNode(const std::string &id, const std::string &description, int interactionOrder)
    : UserInteractionNode(id), name("UserInputIntegerNode") {
  // input ports
  promptMessagePort = createPort<DataPort>(1, PortDirection::In);
  defaultValuePort = createPort<DataPort>(2, PortDirection::In);

  // output ports
  valueOutPort = createPort<DataPort>(1, PortDirection::Out);
  interactions = std::make_unique<InteractionContainer<int>>(getId(), interactionOrder, InteractionDirection::INPUT,
                                                             InteractionType::INT, description);
}

const std::string &UserInputIntegerNode::getName() const {
  return name;
}

json UserInputIntegerNode::getInteractionJson(int loopIteration) const {
  return interactions->getInteraction(loopIteration)->toJson();
}

bool UserInputIntegerNode::needsInteraction(int loopIteration, int executionIteration) const {
  return !interactions->getInteraction(loopIteration)->isCompleted();
}

void UserInputIntegerNode::setInteractionJson(int loopIteration, json interactionJson) const {
  interactions->getInteraction(loopIteration)->fromJson(interactionJson);
}

bool UserInputIntegerNode::processInputPortData(WorkflowExecution *execution) {
  const int loopIteration = execution->getLoopIteration();
  if (promptMessagePort->getData()) {
    auto promptMessage = execution->expandVariables(promptMessagePort->getData()->toString());
    interactions->getInteraction(loopIteration)->setDescription(promptMessage);
  }
  if (defaultValuePort->getData()) {
    auto defaultValue = execution->expandVariables(defaultValuePort->getData()->toString());
    interactions->getInteraction(loopIteration)->setDefaultValue(std::make_unique<int>(StaticData(defaultValue).toInt()));
  }
  if (defaultValuePort->getData()) {
    interactions->getInteraction(loopIteration)->setDefaultValue(std::make_unique<int>(defaultValuePort->getData()->toInt()));
  }
  return true; // no validation of required ports for now
}

bool UserInputIntegerNode::processOutputPortData(WorkflowExecution *execution) {
  const int loopIteration = execution->getLoopIteration();
  if (interactions->getInteraction(loopIteration)->getValue()) {
    valueOutPort->setData(std::make_shared<StaticData>(*interactions->getInteraction(loopIteration)->getValue()));
  }
  return true;
}

void UserInputIntegerNode::initInteraction(int pageNumber, int loopIteration) const {
  interactions->getInteraction(loopIteration)->setPageNumber(pageNumber);
  interactions->getInteraction(loopIteration)->setLoopIteration(loopIteration);
}
