#pragma once

#include "../../interaction/InteractionContainer.h"
#include "../../ports/DataPort.h"
#include "../UserInteractionNode.h"
#include <sstream>

class UserOutputWebViewNode : public UserInteractionNode {

public:
  explicit UserOutputWebViewNode(const std::string &id, const std::string &description, const std::string &url, int interactionOrder);
  const std::string &getName() const override;

protected:
  json getInteractionJson(int loopIteration) const override;
  void setInteractionJson(int loopIteration, json interactionJson) const override;

private:
  bool needsInteraction(int loopIteration, int executionIteration) const override;
  void initInteraction(int pageNumber, int loopIteration) const override;
  bool processInputPortData(WorkflowExecution *execution) override;
  bool processOutputPortData(WorkflowExecution *execution) override;

  std::string name;
  std::unique_ptr<InteractionContainer<std::string>> interactions;
  DataPort *promptMessagePort;
  DataPort *urlPort;
};
