/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "UserOutputTextNode.h"
#include "../../../Logger.h"
#include "../../../util.h"
#include "../../workflow/Workflow.h"

UserOutputTextNode::UserOutputTextNode(const std::string &id, const std::string &inputFile, int interactionOrder)
    : UserInteractionNode(id), name("UserOutputTextNode") {
  // input ports
  inputFilePort = createPort<DataPort>(1, PortDirection::In);

  interactions = std::make_unique<InteractionContainer<std::string>>(getId(), interactionOrder,
                                                                     InteractionDirection::OUTPUT, InteractionType::STRING, "");
}

const std::string &UserOutputTextNode::getName() const {
  return name;
}

json UserOutputTextNode::getInteractionJson(int loopIteration) const {
  return interactions->getInteraction(loopIteration)->toJson();
}

bool UserOutputTextNode::needsInteraction(int loopIteration, int executionIteration) const {
  // return true iff the user has already seen the output
  // this should be the case if he continued the execution after the interactions json was saved (and shown in the gui)
  return executionIteration <= interactions->getInteraction(loopIteration)->getPageNumber();
}

void UserOutputTextNode::setInteractionJson(int loopIteration, json interactionJson) const {
  interactions->getInteraction(loopIteration)->fromJson(interactionJson);
}

bool UserOutputTextNode::processInputPortData(WorkflowExecution *execution) {
  const int loopIteration = execution->getLoopIteration();
  if (inputFilePort->getData()) {
    auto inputFile = execution->expandVariables(inputFilePort->getData()->toString());
    if (inputFile.empty()) {
      LOG("Error: Path for the input file was empty");
      return false;
    }
    std::string path = util::getAbsoluteFilePath(inputFile);
    if (!util::fileExists(path)) {
      LOG("Error: File does not exist");
      return false;
    }
    std::string fileContent = util::readFile(path);
    LOG_VERBOSE("Reading file content (file name: " << path << "): ");
    LOG_VERBOSE(fileContent);
    if (!fileContent.empty()) {
      interactions->getInteraction(loopIteration)->setDescription(fileContent);
      return true;
    } else {
      LOG("Text file was empty!");
    }
  } else {
    LOG("Mandatory input port not connected!");
  }
  return false;
}

bool UserOutputTextNode::processOutputPortData(WorkflowExecution *) {
  return true; // no output ports with data
}

void UserOutputTextNode::initInteraction(int pageNumber, int loopIteration) const {
  interactions->getInteraction(loopIteration)->setPageNumber(pageNumber);
  interactions->getInteraction(loopIteration)->setLoopIteration(loopIteration);
}
