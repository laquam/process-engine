#pragma once

#include <memory>

#include "../../interaction/InteractionContainer.h"
#include "../../ports/DataPort.h"
#include "../UserInteractionNode.h"
#include "../control/BranchingNode.h"

class UserInputChooseNode : public UserInteractionNode {

public:
  explicit UserInputChooseNode(const std::string &id, const std::string &description, int interactionOrder,
                               unsigned int numberOfOptions);

  const std::string &getName() const override;

protected:
  json getInteractionJson(int loopIteration) const override; // called in processInteractions()
  void setInteractionJson(int loopIteration, json interactionJson) const override;

private:
  bool needsInteraction(int loopIteration, int executionIteration) const override;
  void initInteraction(int pageNumber, int loopIteration) const override;
  bool processInputPortData(WorkflowExecution *execution) override;
  bool processOutputPortData(WorkflowExecution *execution) override;

  std::string name;

  // ingoing ports
  DataPort *promptMessagePort;
  DataPort *defaultValuePort;
  std::vector<DataPort *> optionTextPorts; // text labels that the user provided

  // outgoing ports
  DataPort *selectedOptionPort;

  // other
  std::unique_ptr<InteractionContainer<std::string>> interactions;
  const unsigned int numberOfOptions;
};
