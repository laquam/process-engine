/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../../interaction/InteractionContainer.h"
#include "../UserInteractionNode.h"

class UserInputPeriodicTableNode : public UserInteractionNode {

public:
  explicit UserInputPeriodicTableNode(const std::string &id, const std::string &description, int interactionOrder);
  ~UserInputPeriodicTableNode() override = default;

  const std::string &getName() const override;

protected:
  json getInteractionJson(int loopIteration) const override;
  void setInteractionJson(int loopIteration, json interactionJson) const override;

private:
  bool needsInteraction(int loopIteration, int executionIteration) const override;
  void initInteraction(int pageNumber, int loopIteration) const override;
  bool processInputPortData(WorkflowExecution *execution) override;
  bool processOutputPortData(WorkflowExecution *execution) override;

  std::string name;
  std::unique_ptr<InteractionContainer<std::string>> interactions;
  DataPort *promptMessagePort;
  DataPort *defaultValuePort;
  DataPort *valueOutPort;
};
