/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "UserInputFloatNode.h"
#include "../../../Logger.h"
#include "../../workflow/Workflow.h"

UserInputFloatNode::UserInputFloatNode(const std::string &id, const std::string &description, int interactionOrder)
    : UserInteractionNode(id), name("UserInputFloatNode") {
  // input ports
  promptMessagePort = createPort<DataPort>(1, PortDirection::In);
  defaultValuePort = createPort<DataPort>(2, PortDirection::In);

  // output ports
  valueOutPort = createPort<DataPort>(1, PortDirection::Out);
  interactions = std::make_unique<InteractionContainer<float>>(getId(), interactionOrder, InteractionDirection::INPUT,
                                                               InteractionType::FLOAT, description);
}

const std::string &UserInputFloatNode::getName() const {
  return name;
}

json UserInputFloatNode::getInteractionJson(int loopIteration) const {
  return interactions->getInteraction(loopIteration)->toJson();
}

bool UserInputFloatNode::needsInteraction(int loopIteration, int executionIteration) const {
  return !interactions->getInteraction(loopIteration)->isCompleted();
}

void UserInputFloatNode::setInteractionJson(int loopIteration, json interactionJson) const {
  interactions->getInteraction(loopIteration)->fromJson(interactionJson);
}

bool UserInputFloatNode::processInputPortData(WorkflowExecution *execution) {
  const int loopIteration = execution->getLoopIteration();
  if (promptMessagePort->getData()) {
    auto promptMessage = execution->expandVariables(promptMessagePort->getData()->toString());
    interactions->getInteraction(loopIteration)->setDescription(promptMessage);
  }
  if (defaultValuePort->getData()) {
    auto defaultValue = execution->expandVariables(defaultValuePort->getData()->toString());
    interactions->getInteraction(loopIteration)->setDefaultValue(std::make_unique<float>(StaticData(defaultValue).toFloat()));
  }
  return true;
}

bool UserInputFloatNode::processOutputPortData(WorkflowExecution *execution) {
  const int loopIteration = execution->getLoopIteration();
  if (interactions->getInteraction(loopIteration)->getValue()) {
    valueOutPort->setData(std::make_shared<StaticData>(*interactions->getInteraction(loopIteration)->getValue()));
  }
  return true;
}

void UserInputFloatNode::initInteraction(int pageNumber, int loopIteration) const {
  interactions->getInteraction(loopIteration)->setPageNumber(pageNumber);
  interactions->getInteraction(loopIteration)->setLoopIteration(loopIteration);
}
