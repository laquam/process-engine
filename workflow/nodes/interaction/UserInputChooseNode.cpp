#include "UserInputChooseNode.h"

UserInputChooseNode::UserInputChooseNode(const std::string &id, const std::string &description, int interactionOrder,
                                         unsigned int numberOfOptions)
    : UserInteractionNode(id), name("UserInputChooseNode"), numberOfOptions(numberOfOptions) {

  // inputs
  promptMessagePort = createPort<DataPort>(1, PortDirection::In);
  defaultValuePort = createPort<DataPort>(2, PortDirection::In);
  for (int i = 0; i < numberOfOptions; i++) {
    int index = i + 3; // starting at 3
    optionTextPorts.push_back(createPort<DataPort>(index, PortDirection::In));
  }

  // outputs
  selectedOptionPort = createPort<DataPort>(1, PortDirection::Out);

  interactions = std::make_unique<InteractionContainer<std::string>>(getId(),
                                                                     interactionOrder, InteractionDirection::INPUT, InteractionType::CHOOSE, description);
}

json UserInputChooseNode::getInteractionJson(int loopIteration) const {
  return interactions->getInteraction(loopIteration)->toJson();
}

void UserInputChooseNode::setInteractionJson(int loopIteration, json interactionJson) const {
  interactions->getInteraction(loopIteration)->fromJson(interactionJson);
}

bool UserInputChooseNode::needsInteraction(int loopIteration, int executionIteration) const {
  return !interactions->getInteraction(loopIteration)->isCompleted();
}

void UserInputChooseNode::initInteraction(int pageNumber, int loopIteration) const {
  interactions->getInteraction(loopIteration)->setPageNumber(pageNumber);
  interactions->getInteraction(loopIteration)->setLoopIteration(loopIteration);
}

bool UserInputChooseNode::processInputPortData(WorkflowExecution *execution) {
  const int loopIteration = execution->getLoopIteration();
  Interaction<std::string> *interaction = interactions->getInteraction(loopIteration);
  if (promptMessagePort->getData()) {
    auto promptMessage = execution->expandVariables(promptMessagePort->getData()->toString());
    interaction->setDescription(promptMessage);
  }
  if (defaultValuePort->getData()) {
    int defaultSelection = StaticData(execution->expandVariables(defaultValuePort->getData()->toString())).toInt();
    if (defaultSelection < optionTextPorts.size() && optionTextPorts[defaultSelection]->getData()) {
      interactions->getInteraction(loopIteration)->setDefaultValue(std::make_unique<std::string>(optionTextPorts[defaultSelection]->getData()->toString()));
    }
  }
  std::vector<std::string> options;
  for (const auto &optionTextPort : optionTextPorts) {
    if (optionTextPort) {
      auto data = optionTextPort->getData();
      if (data && !data->isEmpty()) {
        options.push_back(data->toString());
      }
    }
  }
  interaction->setOptions(options);
  return true;
}

bool UserInputChooseNode::processOutputPortData(WorkflowExecution *execution) {
  const int loopIteration = execution->getLoopIteration();
  std::string *interactionValue = interactions->getInteraction(loopIteration)->getValue();
  if (interactionValue) {
    int outputValue = -1;
    std::string selectedOption = std::string(*interactionValue);
    for (int i = 0; i < optionTextPorts.size(); i++) {
      if (optionTextPorts[i]) {
        auto data = optionTextPorts[i]->getData();
        if (data && !data->isEmpty() && data->toString() == selectedOption) {
          outputValue = i;
          break; // take the first match, ambiguous values will be ignored
        }
      }
    }
    outputValue++; // in the workflow editor, values start from 1
    selectedOptionPort->setData(std::make_unique<StaticData>(outputValue));
  }
  return true;
}

const std::string &UserInputChooseNode::getName() const {
  return name;
}
