/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../interaction/Interaction.h"
#include "../ports/DependencyPort.h"
#include "../ports/PipePort.h"

#include "../state/WorkflowExecution.h"
#include "ExecutableNode.h"

class UserInteractionNode : public ExecutableNode {
public:
  explicit UserInteractionNode(const std::string &id);
  virtual ~UserInteractionNode() = default;

  bool execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) override;
  void postExecutionHook(WorkflowExecution *execution, bool dry, bool detach, int iteration) override;

  virtual bool processInputPortData(WorkflowExecution *execution) = 0;
  virtual bool processOutputPortData(WorkflowExecution *execution) = 0;
  // note: deriving classes must also override pure virtual methods from Node:
  // - const std::string & getName() const
protected:
  virtual json getInteractionJson(int loopIteration) const = 0; // called in processInteractions()
  virtual void setInteractionJson(int loopIteration, json interactionJson) const = 0;

private:
  // This method is called by UserInteractionNode::processInteractions() to determine if the execution state of this
  // node must be set to true (as soon as no more user interaction is necessary)
  virtual bool needsInteraction(int loopIteration, int executionIteration) const = 0;

  // idea: implement a template-free interface for interaction with initInteraction(int, int),
  // so that this abstract method is not necessary
  virtual void initInteraction(int pageNumber, int loopIteration) const = 0;
  void processInteractions(WorkflowNodeStates *nodeStates, bool dry, int iteration, int loopIteration);
};
