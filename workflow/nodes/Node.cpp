/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../../Logger.h"
#include "../ports/DependencyPort.h"
#include "../ports/PipePort.h"
#include <algorithm>
#include <cassert>
#include <unordered_set>

#include "Node.h"

void Node::addPort(std::unique_ptr<Port> port) {
  if (!port) {
    throw std::invalid_argument("Can not add a port to the node which is null");
  }
  port->setNodeId(getId());
  port->setNode(this);
  port->setWorkflow(workflow);
  auto invalidIndexMessage = "Port index " + std::to_string(port->getIndex()) + " is already taken!";
  if (port->getDirection() == PortDirection::In) {
    if (ports_in.find(port->getIndex()) != ports_in.end()) {
      throw std::invalid_argument(invalidIndexMessage);
    }
    ports_in[port->getIndex()] = std::move(port);
  } else if (port->getDirection() == PortDirection::Out) {
    if (ports_out.find(port->getIndex()) != ports_out.end()) {
      throw std::invalid_argument(invalidIndexMessage);
    }
    ports_out[port->getIndex()] = std::move(port);
  } else {
    throw std::logic_error("Port could not be added to Node: Unknown direction");
  }
}

Port *Node::getPort(PortDirection direction, int index) const {
  if (direction == PortDirection::In) {
    if (ports_in.find(index) != ports_in.end()) {
      return ports_in.at(index).get();
    }
  } else if (direction == PortDirection::Out) {
    if (ports_out.find(index) != ports_out.end()) {
      return ports_out.at(index).get();
    }
  } else {
    throw std::invalid_argument("Unknown direction");
  }
  return {};
}

const std::string &Node::getId() const {
  return id;
}

std::unordered_set<std::string> Node::getAllDependencies() const {
  std::unordered_set<std::string> dependencies;
  for (auto &port : ports_in) {
    auto newDependencies = port.second->getDependencies();
    for (const auto &dependencyNodeId : newDependencies) {
      dependencies.insert(dependencyNodeId);
    }
  }

  return dependencies;
}


std::vector<CoherentConnectionInfo> Node::getCoherentConnections(PortDirection direction) const {
  std::vector<CoherentConnectionInfo> result;
  if (!(direction == PortDirection::Out || direction == PortDirection::In)) {
    throw std::logic_error("Unknown Port Direction");
  }
  processPortsOfType<CoherencyPort>(direction, [&](const CoherencyPort *port) {
    auto connectedPorts = port->getConnectedPorts();
    for (const Port *connectedPort : connectedPorts) {
      if (!connectedPort) {
        throw std::logic_error("Connected port is null");
      }
      auto connectedCoherencyPort = dynamic_cast<const CoherencyPort *>(connectedPort);
      CoherentConnectionType left = port->getCoherentConnectionType();
      CoherentConnectionType right = CoherentConnectionType::NONE;
      if (connectedCoherencyPort) {
        right = connectedCoherencyPort->getCoherentConnectionType();
      }
      CoherentConnectionType connectionType = resultingCoherentConnectionType(left, right);
      result.push_back(CoherentConnectionInfo{connectedPort->getNodeId(), connectionType});
    }
  });
  return std::move(result);
}

void Node::setWorkflow(Workflow *workflow) {
  this->workflow = workflow;
  for (const auto &item : ports_in) {
    item.second->setWorkflow(workflow);
  }
  for (const auto &item : ports_out) {
    item.second->setWorkflow(workflow);
  }
}

NodeState Node::getInitialState() const {
  return NodeState{NodeExecutionState::TO_BE_EXECUTED, 0};
}

std::unordered_set<std::string> Node::getExplicitDependents() const {
  return getConnectedIds(ports_out, true);
}

std::unordered_set<std::string>
Node::getConnectedIds(const std::unordered_map<int, std::unique_ptr<Port>> &portList, bool onlyDependencyPorts) {
  std::unordered_set<std::string> result;
  for (auto &port : portList) {
    auto connectedPorts = port.second->getConnectedPorts();
    for (const Port *connectedPort : connectedPorts) {
      if (connectedPort) {
        if (onlyDependencyPorts && !dynamic_cast<const DependencyPort *>(connectedPort)) {
          continue;
        }
        auto dependencyId = connectedPort->getNodeId();
        result.insert(dependencyId);
      }
    }
  }
  return result;
}

void Node::postExecutionHook(WorkflowExecution *, bool, bool, int) {
  // empty super implementation of optional hook
}

bool Node::processInputPortData(WorkflowExecution *) {
  return true;
}

bool Node::processOutputPortData(WorkflowExecution *) {
  return true;
}

CoherentConnectionType
Node::resultingCoherentConnectionType(CoherentConnectionType left, CoherentConnectionType right) {
  if (left == right) {
    return left;
  } else if (left == CoherentConnectionType::PIPE && right != CoherentConnectionType::PIPE) {
    return CoherentConnectionType::PIPE_TO_OTHER;
  }
  return CoherentConnectionType::INVALID;
}
