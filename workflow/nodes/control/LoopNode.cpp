#include "../../../Logger.h"
#include "../../Workflow.h"
#include "../../WorkflowBranch.h"
#include "../../WorkflowExecutor.h"
#include "../../ports/BranchingPort.h"

#include "LoopNode.h"


const std::string LoopNode::LOOP_INDEX_JSON_KEY = "loopIndex";

LoopNode::LoopNode(std::string id) : BranchingNode(std::move(id)), name("Loop"), loopCounter(0),
                                     condition(false), conditionSet(false), startIndex(0), endIndex(0),
                                     endIndexSet(false), step(1),
                                     varName(config::DEFAULT_LOOP_VARIABLE) {
  // inputs
  createPort<DependencyPort>(0, PortDirection::In);
  conditionPort = createPort<DataPort>(1, PortDirection::In);
  startIndexPort = createPort<DataPort>(2, PortDirection::In);
  endIndexPort = createPort<DataPort>(3, PortDirection::In);
  stepPort = createPort<DataPort>(4, PortDirection::In);
  varNamePort = createPort<DataPort>(5, PortDirection::In);

  startIndexPort->setData(std::make_shared<StaticData>(0)); // default value

  // outputs
  createPort<DependencyPort>(0, PortDirection::Out);
  loopBranchPort = createPort<BranchingPort>(1);
  loopIndexPort = createPort<DataPort>(2, PortDirection::Out);
}

bool LoopNode::execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) {
  WorkflowNodeStates *nodeStates = execution->getNodeStates();
  WorkflowBranch *branch = loopBranchPort->getBranch();
  if (!branch) throw std::logic_error("Branches must be initialized");
  if (!endIndexSet && !conditionSet) {
    throw std::invalid_argument("Loop node has no valid exit condition");
  }

  // return true and set state to executed by default (for example if there is no next loop iteration)
  NodeState previousNodeState = nodeStates->getNodeState(getId());
  NodeState resultState = NodeState{NodeExecutionState::EXECUTED, previousNodeState.successfulExecutions,
                                    previousNodeState.customInfo};
  bool success = true;

  // restore the loop index from persistence
  loopCounter = previousNodeState.customInfo[LOOP_INDEX_JSON_KEY].get<unsigned int>();

  if (!loopCondition() && firstIteration()) {
    // the loop branch was not and will not be executed => skip it
    execution->skipBranch(branch->getIdentifier());
    nodeStates->setNodeState(getId(), resultState);
    return success;
  }

  for (; loopCondition() && resultState.currentState == NodeExecutionState::EXECUTED; loopCounter++) {

    // the loop index must be set now, so that nodes inside the loop can use it
    processOutputPortData(execution);

    LOG("Executing loop iteration #" << loopCounter << " with index " << loopIndex() << std::endl);

    WorkflowExecution *branchExecution;
    if (branch->isEmpty()) {
      LOG("The loop is empty, nothing to do!");
    } else {
      branchExecution = execution->getBranchExecution(branch->getIdentifier());

      // the loopIteration will be used to distinguish between multiple interactions with the same nodeId
      branchExecution->setLoopIteration(static_cast<int>(loopCounter)); //TODO refactor setLoopIteration to uint

      if (!firstIteration()) {
        // reset node states to each nodes' default state, but keep counters intact
        // otherwise they won't be executed because the state still is set to "EXECUTED" for instance
        branchExecution->getNodeStates()->reset(true);
      } else {
        branchExecution->updateNodeStatesFromParent();
        branchExecution->getNodeStates()->reset(false);
      }

      success = WorkflowExecutor::execute(branch, branchExecution, dry, detach, true);

      // report back the results of this iteration (execution might suspend when there are interactions in the loop)
      execution->branchExecutionFinished(branch->getIdentifier());
      resultState.currentState = executionStateToNodeState(branchExecution->getState());
    }
    if (resultState.currentState == NodeExecutionState::EXECUTED) {
      resultState.customInfo[LOOP_INDEX_JSON_KEY] = loopCounter + 1;
    }

    // update the data from the input ports which changed input must be considered
    // it is especially important to re-expand any variables that they contain because the variables value can have changed
    auto conditionData = conditionPort->getData();
    if (conditionData) {
      conditionSet = true;
      condition = conditionData->expandBool(execution);
    }
    auto endIndexData = endIndexPort->getData();
    if (endIndexData) {
      endIndexSet = true;
      endIndex = endIndexData->expandInt(execution);
    }
  }

  nodeStates->setNodeState(getId(), resultState);
  return success;
}

const std::string &LoopNode::getName() const {
  return name;
}

std::function<bool(const Node *)> LoopNode::getBranchExitCondition() {
  return [=](const Node *node) {
    // end branch when an EndLoopNode is reached
    return (dynamic_cast<const LoopNode *>(node) != nullptr) && node->getId() == getId();
  };
}

bool LoopNode::loopCondition() {
  bool result = true;
  if (endIndexSet) {
    if (loopIndex() > endIndex) { // the loop should still run once with index == endIndex!
      result = false;
      LOG("Exiting loop: Loop index is greater than to the value of endIndex");
    }
  } else {
    LOG_VERBOSE("No range is given, ignoring range for loop condition check");
  }
  if (conditionSet) {
    if (!condition) {
      LOG("Exiting loop: Loop condition is false");
      result = false;
    }
  } else {
    LOG_VERBOSE("No loop condition is given");
  }
  return result;
}

NodeState LoopNode::getInitialState() const {
  NodeState result = ExecutableNode::getInitialState();
  result.customInfo = {{LOOP_INDEX_JSON_KEY, 0}};
  return result;
}

bool LoopNode::firstIteration() const {
  return (loopCounter == 0);
}

bool LoopNode::processInputPortData(WorkflowExecution *execution) {
  if (startIndexPort->getData()) {
    startIndex = startIndexPort->getData()->expandInt(execution);
  }
  varName = config::DEFAULT_LOOP_VARIABLE;
  if (varNamePort->getData() && !varNamePort->getData()->isEmpty()) {
    varName = varNamePort->getData()->toString();
  }
  auto conditionData = conditionPort->getData();
  if (conditionData) {
    conditionSet = true;
    condition = conditionData->expandBool(execution);
  }
  auto endIndexData = endIndexPort->getData();
  if (endIndexData) {
    endIndexSet = true;
    endIndex = endIndexData->expandInt(execution);
  }
  auto stepData = stepPort->getData();
  if (stepData) {
    step = stepData->expandInt(execution);
  }
  return true;
}

bool LoopNode::processOutputPortData(WorkflowExecution *execution) {
  int index = std::min(loopIndex(), endIndex);
  execution->setVariable(varName, std::to_string(index));
  loopIndexPort->setData(std::make_shared<StaticData>(index));
  return true;
}

int LoopNode::loopIndex() const {
  // note: step (and startIndex) must be constant for this calculation to be correct!
  return static_cast<int>(startIndex + (loopCounter * step));
}
