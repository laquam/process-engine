/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "BranchSelectNode.h"
#include "../../../Logger.h"
#include "../../WorkflowExecutor.h"

BranchSelectNode::BranchSelectNode(std::string id, unsigned int numberOfBranches)
    : BranchingNode(std::move(id)), name("BranchSelect"), numberOfBranches(numberOfBranches) {

  // inputs
  createPort<DependencyPort>(0, PortDirection::In);
  selectPort = createPort<DataPort>(1, PortDirection::In);

  // outputs
  createPort<DependencyPort>(0, PortDirection::Out);
  for (int i = 1; i <= numberOfBranches; i++) {
    branchingPorts.push_back(createPort<BranchingPort>(i));
  }
}

bool BranchSelectNode::execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) {
  WorkflowNodeStates *nodeStates = execution->getNodeStates();
  WorkflowBranch *selectedBranch;
  bool success = true;
  if (selectPort->getData()) {
    unsigned int selectedBranchNumber = selectPort->getData()->toInt();
    selectedBranchNumber -= 1; // in the workflow editor, the numbering (of branches) starts from 1
    if (selectedBranchNumber < 0 || selectedBranchNumber > branchingPorts.size()) {
      LOG("There is no branch with the number " << selectedBranchNumber + 1);
      nodeStates->updateNodeExecutionState(getId(), NodeExecutionState::ERROR);
      return false;
    }
    selectedBranch = branchingPorts[selectedBranchNumber]->getBranch();
    LOG("Selected branch: " << selectedBranchNumber + 1 << " (skipping other branches)");

    if (!selectedBranch) {
      LOG("Unable to execute the selected branch");
      success = false;
    }
    for (int i = 0; i < branchingPorts.size(); i++) {
      if (!branchingPorts[i]) continue;
      auto branch = branchingPorts[i]->getBranch();
      if (branch && i != selectedBranchNumber) {
        execution->skipBranch(branch->getIdentifier());
      }
    }
    NodeExecutionState result = NodeExecutionState::EXECUTED;
    if (success && !selectedBranch->isEmpty()) {
      WorkflowExecution *branchExecution = execution->getBranchExecution(selectedBranch->getIdentifier());
      branchExecution->updateNodeStatesFromParent();
      success = WorkflowExecutor::execute(selectedBranch, branchExecution, dry, detach, true);
      execution->branchExecutionFinished(selectedBranch->getIdentifier());
      result = executionStateToNodeState(branchExecution->getState());
    }
    nodeStates->updateNodeExecutionState(getId(), result);
  }
  return success;
}

const std::string &BranchSelectNode::getName() const {
  return name;
}
