/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "BranchingNode.h"
#include "../../Workflow.h"
#include "../../WorkflowAnalyzer.h"
#include "../../ports/BranchingPort.h"

BranchingNode::BranchingNode(const std::string &id)
    : ExecutableNode(id), branchesInitialized(false) {
}

void BranchingNode::initBranches(WorkflowExecution *execution) {
  WorkflowAnalyzer analyzer(workflow);
  for (const auto &item : ports_out) {
    int outgoingPortIndex = item.first;
    auto branchingPort = dynamic_cast<BranchingPort *>(item.second.get());
    if (branchingPort) {
      if (!branchesInitialized) {
        auto branch = analyzer.findBranch(workflow, getId(), outgoingPortIndex, getBranchExitCondition());
        execution->registerBranch(branch.get());
        branchingPort->setBranch(std::move(branch));
      } else {
        // the port already has it's branch assigned, but the execution (sub execution) still needs the registration
        auto branchPtr = branchingPort->getBranch();
        if (!branchPtr) throw std::logic_error("Branches of BranchingPort not initialized correctly or invalidated");
        execution->registerBranch(branchPtr);
      }
    }
  }
  branchesInitialized = true;
}

std::function<bool(const Node *)> BranchingNode::getBranchExitCondition() {
  return [](const Node *) { return false; };
}

NodeExecutionState BranchingNode::executionStateToNodeState(ExecutionState branchResult) {
  NodeExecutionState result = NodeExecutionState::NOT_SET;
  switch (branchResult) {
    case ExecutionState::ERROR:
      result = NodeExecutionState::ERROR;
      break;
    case ExecutionState::NEEDS_INTERACTION:
      result = NodeExecutionState::NEEDS_INTERACTION;
      break;
    case ExecutionState::FINISHED:
      result = NodeExecutionState::EXECUTED;
      break;
    default:
      break;
  }
  return result;
}

void BranchingNode::postExecutionHook(WorkflowExecution *execution, bool dry, bool detach, int iteration) {
  if (execution->getNodeState(getId()).currentState == NodeExecutionState::NEEDS_INTERACTION) {
    // perform a normal execution if the current state is NEEDS_INTERACTION
    // this is necessary to reprocess interaction nodes when the workflow is being continued
    execute(execution, dry, detach, iteration);
  }
}
