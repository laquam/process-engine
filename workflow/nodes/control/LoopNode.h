#pragma once


#include "../../ports/DataPort.h"
#include "../../ports/DependencyPort.h"
#include "../../state/WorkflowExecution.h"
#include "../Node.h"
#include "BranchingNode.h"

class BranchingPort;

class LoopNode : public BranchingNode {
public:
  explicit LoopNode(std::string id);

  bool execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) override;
  const std::string &getName() const override;

  NodeState getInitialState() const override;
  bool processInputPortData(WorkflowExecution *execution) override;
  bool processOutputPortData(WorkflowExecution *execution) override;

private:
  std::function<bool(const Node *)> getBranchExitCondition() override;

  bool loopCondition();
  bool firstIteration() const;

  const std::string name;

  const static std::string LOOP_INDEX_JSON_KEY;

  /**
   * internal counter, iterated once every loop iteration
   */
  unsigned int loopCounter;

  /**
   * This function calculates the dynamic loop index which starts at startIndexPort and is incremented by `step`
   * after each loop iteration.
   * This is the value which is populated to the loop output port (loopIndexPort) and to the index variable.
   *
   * Note: The current implementation relies on startIndex and step being constant throughout all loop iterations!
   */
  int loopIndex() const;

  bool condition;
  bool conditionSet;
  int startIndex;
  int endIndex;
  bool endIndexSet;
  int step;
  std::string varName;

  // input ports
  DataPort *conditionPort;
  DataPort *startIndexPort;
  DataPort *endIndexPort;
  DataPort *stepPort;
  DataPort *varNamePort;

  // output ports
  BranchingPort *loopBranchPort;
  DataPort *loopIndexPort;
};
