/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "IfNode.h"
#include "../../../Logger.h"
#include "../../Workflow.h"
#include "../../WorkflowBranch.h"
#include "../../WorkflowExecutor.h"
#include "../../ports/BranchingPort.h"

IfNode::IfNode(std::string id) : BranchingNode(std::move(id)), name("If") {

  // inputs
  createPort<DependencyPort>(0, PortDirection::In);
  conditionPort = createPort<DataPort>(1, PortDirection::In);

  // outputs
  createPort<DependencyPort>(0, PortDirection::Out);
  trueBranchPort = createPort<BranchingPort>(1);
  falseBranchPort = createPort<BranchingPort>(2);
}

bool IfNode::execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) {
  WorkflowNodeStates *nodeStates = execution->getNodeStates();
  WorkflowBranch *selectedBranch;
  WorkflowBranch *branchToSkip;
  bool success = true;
  if (conditionPort->getData() && conditionPort->getData()->toBoolean()) {
    LOG("The condition is true, executing respective branch");
    selectedBranch = trueBranchPort->getBranch();
    branchToSkip = falseBranchPort->getBranch();
  } else {
    LOG("The condition is false or not specified, executing respective branch");
    selectedBranch = falseBranchPort->getBranch();
    branchToSkip = trueBranchPort->getBranch();
  }

  if (!selectedBranch) {
    LOG("Unable to execute the selected branch");
    success = false;
  }
  if (branchToSkip) {
    // tell execution to skip this branch entirely
    execution->skipBranch(branchToSkip->getIdentifier());
  }

  NodeExecutionState result = NodeExecutionState::EXECUTED;
  if (success && !selectedBranch->isEmpty()) {
    WorkflowExecution *branchExecution = execution->getBranchExecution(selectedBranch->getIdentifier());
    branchExecution->updateNodeStatesFromParent();
    success = WorkflowExecutor::execute(selectedBranch, branchExecution, dry, detach, true);
    execution->branchExecutionFinished(selectedBranch->getIdentifier());
    result = executionStateToNodeState(branchExecution->getState());
  }
  nodeStates->updateNodeExecutionState(getId(), result);
  return success;
}

const std::string &IfNode::getName() const {
  return name;
}
