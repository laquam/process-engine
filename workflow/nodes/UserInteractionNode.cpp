/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "UserInteractionNode.h"
#include "../../Logger.h"
#include "../../config.h"
#include "../../util.h"
#include "../ports/ParameterPort.h"
#include "../state/WorkflowNodeStates.h"
#include <fstream>
#include <iomanip>

UserInteractionNode::UserInteractionNode(const std::string &id) : ExecutableNode(id) {
  createPort<DependencyPort>(0, PortDirection::In);
  createPort<DependencyPort>(0, PortDirection::Out);
}

void UserInteractionNode::processInteractions(WorkflowNodeStates *nodeStates, bool dry, int iteration, int loopIteration) {

  /* write interaction info to the interaction description file to allow automatic creation of GUI elements for
     user interaction */
  if (!dry) {
    // open if existing
    json jsonObj;
    if (util::fileExists(config::INTERACTION_FILENAME)) {
      std::ifstream in(config::INTERACTION_FILENAME);
      in >> jsonObj;
    }

    if (!jsonObj.contains(config::INTERACTION_ARRAY_KEY)) {
      jsonObj[config::INTERACTION_ARRAY_KEY] = json::array();
    }

    bool previousDescriptionFound = false;
    const std::string &interactionId = Interaction<void>::createId(getId(), loopIteration);
    for (const auto &interactionJson : jsonObj[config::INTERACTION_ARRAY_KEY]) {
      bool match = interactionJson.contains("id") && interactionJson["id"].get<std::string>() == interactionId;
      if (match) {
        previousDescriptionFound = true;

        LOG_VERBOSE("++ Restoring interaction description: " << interactionJson);
        setInteractionJson(loopIteration, interactionJson); // implemented in subclasses
        break;
      }
    }

    if (!previousDescriptionFound) {
      initInteraction(iteration, loopIteration);
      jsonObj[config::INTERACTION_ARRAY_KEY].push_back(getInteractionJson(loopIteration));

      // save json file
      std::ofstream out(config::INTERACTION_FILENAME);
      out << std::setw(4) << jsonObj << std::endl;
      LOG_VERBOSE("Wrote interaction info to " << config::INTERACTION_FILENAME);
    }
  }

  NodeState previousState = nodeStates->getNodeState(getId());
  if (needsInteraction(loopIteration, iteration)) {
    LOG_VERBOSE(getName() << ": Still needs interaction");
    nodeStates->updateNodeExecutionState(getId(), NodeExecutionState::NEEDS_INTERACTION);
  } else {
    LOG_VERBOSE(getName() << ": Considering this interaction node executed");
    nodeStates->updateNodeExecutionState(getId(), NodeExecutionState::EXECUTED);
  }
}

bool UserInteractionNode::execute(WorkflowExecution *execution, bool dry, bool, int iteration) {
  try {
    processInteractions(execution->getNodeStates(), dry, iteration, execution->getLoopIteration());
    return true;
  } catch (std::exception &exception) {
    LOG("Error while processing interaction: " + std::string(exception.what()));
    return false;
  }
}

void UserInteractionNode::postExecutionHook(WorkflowExecution *execution, bool dry, bool detach, int iteration) {
  // TODO refactor methods so that only loading the interaction and processessing output ports
  // is possible here
  processInputPortData(execution);
  execute(execution, dry, detach, iteration);
  processOutputPortData(execution);
}
