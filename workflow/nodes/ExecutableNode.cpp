#include "ExecutableNode.h"

ExecutableNode::ExecutableNode(const std::string &id) : Node(id), executionProfile(ExecutionProfile::DEFAULT) {
}

void ExecutableNode::setExecutionProfile(ExecutionProfile profile) {
  executionProfile = profile;
}

NodeState ExecutableNode::getInitialState() const {
  if (executionProfile == ExecutionProfile::SKIP) {
    return {NodeExecutionState::TO_BE_SKIPPED, 0};
  } else {
    return Node::getInitialState();
  }
}

ExecutionProfile ExecutableNode::getExecutionProfile() const {
  return executionProfile;
}
