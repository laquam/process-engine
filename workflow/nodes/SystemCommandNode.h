/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include "../CoherentNodeGroup.h"
#include "../state/WorkflowExecution.h"
#include "ExecutableNode.h"

class SystemCommandNode : public ExecutableNode {

public:
  explicit SystemCommandNode(const std::string &id) : ExecutableNode(id) {}
  virtual ~SystemCommandNode() = default;

  virtual std::string getCommand(const WorkflowExecution *execution) const = 0;

  virtual bool execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) override;

  CoherentNodeGroup *getCoherentGroup() const;
  void setCoherentGroup(std::shared_ptr<CoherentNodeGroup> group);
  virtual std::unordered_set<std::string> getAllDependencies() const override;

  /**
   * Return direct dependencies of this node, not including dependencies of other group members
   * @return
   */
  virtual std::unordered_set<std::string> getDirectDependencies() const;

  bool isInCoherentGroup() const;
  NodeState getInitialState() const override;
  void postExecutionHook(WorkflowExecution *execution, bool dry, bool detach, int iteration) override;

private:
  bool executeCommandInternal(WorkflowExecution *execution, const std::string &command, bool dry, bool);
  bool executeSingleNode(WorkflowExecution *execution, bool dry, bool detach);
  bool executeCoherentNodes(WorkflowExecution *execution, bool dry, bool detach);

  void setStdoutOutgoing(const std::string &stdout_output);
  std::pair<bool, std::string> restoreStdoutData(WorkflowExecution *execution);
  void saveStdoutData(WorkflowExecution *execution, const std::string &stdout_output);

  void printCommandLog(const std::string &command);

  std::shared_ptr<CoherentNodeGroup> coherentGroup;
};
