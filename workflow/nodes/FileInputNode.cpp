/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "FileInputNode.h"
#include "../Workflow.h"
#include "../ports/DependencyPort.h"
#include "../ports/PipePort.h"

FileInputNode::FileInputNode(std::string id, std::string path)
    : SystemCommandNode(std::move(id)), path(std::move(path)), name("FileInputNode") {
  createPort<DependencyPort>(0, PortDirection::In);
  filePathPort = createPort<DataPort>(1, PortDirection::In);
  createPort<DependencyPort>(0, PortDirection::Out);
  createPort<PipePort>(1, PortDirection::Out);
}

const std::string &FileInputNode::getName() const {
  return name;
}

std::string FileInputNode::getCommand(const WorkflowExecution *execution) const {
  std::string filePath = path;
  auto portData = filePathPort->getData();
  if (portData && !portData->isEmpty()) {
    filePath = portData->toString();
  }
  std::vector<CoherentConnectionInfo> outgoingCoherentConnections = getCoherentConnections(PortDirection::Out);
  if (!outgoingCoherentConnections.empty() && outgoingCoherentConnections[0].connectionType != CoherentConnectionType::PIPE_TO_OTHER) {
    return "cat " + execution->expandVariables(filePath);
  }
  return {};
}
