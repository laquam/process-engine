#pragma once

#include "../state/ExecutionProfile.h"
#include "Node.h"

class ExecutableNode : public Node {
public:
  explicit ExecutableNode(const std::string &id);
  ~ExecutableNode() override = default;

  bool execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) override = 0;
  void setExecutionProfile(ExecutionProfile profile);
  NodeState getInitialState() const override;

  ExecutionProfile getExecutionProfile() const;

protected:
  ExecutionProfile executionProfile;
};
