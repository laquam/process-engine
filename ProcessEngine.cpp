/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "Logger.h"
#include "config.h"
#include "util.h"
#include <fstream>
#include <iomanip>
#include <utility>
#include <zconf.h>

#include "ProcessEngine.h"
#include "workflow/WorkflowExecutor.h"
#include "workflow/parse/JsonParser.h"
#include "workflow/state/WorkflowExecution.h"

using json = nlohmann::json;


bool ProcessEngine::runWorkflow(const std::string &workflowFile, bool dry, bool detached) {
  JsonParser parser(workflowFile);
  try {
    workflow = parser.parseJsonWorkflow();
  } catch (const std::exception &e) {
    throw std::invalid_argument(std::string("Error while parsing given workflow: ") + e.what());
  }

  pid_t pid = getpid();

  if (!dry) {
    if (!createFolders()) {
      std::cerr << "Error: Unable to create the necessary folders for this workflow."
                   " No permission, invalid path or folder already existing?"
                << std::endl;
    }
  }

  currentExecution = std::make_unique<WorkflowExecution>(workflow.get(), true);
  currentExecution->setFileName(workflowFile);
  currentExecution->setPid(pid);
  currentExecution->setState(ExecutionState::RUNNING);
  currentExecution->setStartDateTime(util::getCurrentDateTime());

  setDefaultVariables(currentExecution.get());

  bool executionSuccess = false;
  if (detached) {
    pid = fork();
    if (pid > 0) {
      // parent process: print json info and exit
      currentExecution->setPid(pid); // set pid to child process for printing and persisting
      currentExecution->persistInfo();
      std::cout << std::setw(4) << currentExecution->toJson() << std::endl;
      exit(0);
    } else {
      // child process: run independently in the background, detached from the parent.

      GetLogger().setPrintLog(true);
      GetLogger().setPrintStdout(false);

      // as this runs in the forked child process the pid in the workflow execution needs to be updated
      currentExecution->setPid(getpid());
    }
  }
  try {
    executionSuccess = WorkflowExecutor::execute(workflow.get(), currentExecution.get(), dry, detached, false);
  } catch (std::exception &error) {
    LOG("Error while executing the workflow: " << error.what());
    currentExecution->setState(ExecutionState::ERROR);
  }
  LOG("");
  LOG(std::string(15, '-') << " EXECUTION RESULT " << std::string(15, '-'));
  if (executionSuccess) {
    LOG("OK: Workflow execution finished");
  } else {
    LOG("ERROR: Execution stopped on error");
  }
  LOG("New state: " << executionStateToString(currentExecution->getState()));
  LOG("Nodes to process: " << currentExecution->getNodesToProcess());
  LOG("Nodes processed: " << currentExecution->getNodesProcessed());

  if (currentExecution->getState() == ExecutionState::FINISHED || currentExecution->getState() == ExecutionState::ERROR) {
    // workflow execution will not be continued
    if (!util::deleteFolder(config::TEMPORARY_FOLDER)) {
      LOG("WARNING: Unable to delete temp folder (" << config::TEMPORARY_FOLDER << ").");
    }
    currentExecution->setEndDateTime(util::getCurrentDateTime());
    currentExecution->persistInfo();
  }

  return executionSuccess;
}

bool ProcessEngine::continueWorkflow() {
  return signalRunningEngine(SIGUSR1);
}

void ProcessEngine::printWorkflowStatus() {
  // the infoFile is updated by the process which handles the execution
  // it can be read from the filesystem to get the requested information
  json currentStatus = restoreStatus(); // to get the pid

  if (currentStatus.contains(config::PID_KEY)) {
    pid_t pid_of_running_process_engine;
    pid_of_running_process_engine = currentStatus[config::PID_KEY];
    if (!util::processRunning(pid_of_running_process_engine)) {
      // no active process, set state to CANCELLED

      currentStatus["state"] = executionStateToString(ExecutionState::CANCELLED);

      std::ofstream out(config::INFO_FILENAME);
      out << std::setw(4) << currentStatus << std::endl;
    }
  }

  std::cout << std::setw(4) << currentStatus << std::endl;
}

bool ProcessEngine::createFolders() {
  bool success = true;

  if (!util::createDirectory(config::LOG_FOLDER)) {
    std::cerr << "Could not create log directory in path " << path << std::endl;
    success = false;
  }

  if (!util::createDirectory(config::TEMPORARY_FOLDER)) {
    std::cerr << "Could not create log directory in path " << path << std::endl;
    success = false;
  }

  return success;
}

void ProcessEngine::printWorkflowLog() {
  std::string logPath = config::LOG_FOLDER + "/" + config::LOG_FILENAME;
  if (!logPath.empty() && util::fileExists(logPath)) {
    std::cout << util::readFile(logPath) << std::endl;
  }
}

void ProcessEngine::printWorkflowShortcuts() {
  if (!config::SHORTCUT_FILENAME.empty() && util::fileExists(config::SHORTCUT_FILENAME)) {
    std::cout << util::readFile(config::SHORTCUT_FILENAME) << std::endl;
  }
}

void ProcessEngine::printInteractions() {
  if (!config::INTERACTION_FILENAME.empty() && util::fileExists(config::INTERACTION_FILENAME)) {
    std::cout << util::readFile(config::INTERACTION_FILENAME) << std::endl;
  }
}

bool ProcessEngine::inputInteractionValue(const std::string &interactionId, const std::string &inputValue) {
  bool valueWritten = false;
  std::ifstream in(config::INTERACTION_FILENAME);
  json inJsonObject, outJsonObject;
  try {
    in >> inJsonObject;
    if (inJsonObject.contains(config::INTERACTION_ARRAY_KEY) && inJsonObject[config::INTERACTION_ARRAY_KEY].is_array()) {
      // copy each interaction json object, insert the value in case the one matching id was found
      for (const auto &interactionObj : inJsonObject[config::INTERACTION_ARRAY_KEY]) {
        if (interactionObj.contains("id") && interactionObj["id"].get<std::string>() == interactionId) {
          json interactionObjCopy = interactionObj;
          if (interactionObjCopy["type"] == "int") {
            interactionObjCopy["value"] = std::stoi(inputValue);
          } else if (interactionObjCopy["type"] == "float") {
            std::string value(inputValue);
            std::replace(value.begin(), value.end(), ',', '.');
            interactionObjCopy["value"] = std::stof(value);
          } else if (interactionObjCopy["type"] == "bool") {
            if (inputValue == "true" || inputValue == "True") {
              interactionObjCopy["value"] = true;
            } else {
              interactionObjCopy["value"] = false;
            }
          } else {
            interactionObjCopy["value"] = inputValue;
          }
          if (valueWritten) {
            std::cerr << "Interaction id matched more than one entries" << std::endl;
            return false;
          }
          outJsonObject[config::INTERACTION_ARRAY_KEY].push_back(interactionObjCopy);
          valueWritten = true;
        } else {
          outJsonObject[config::INTERACTION_ARRAY_KEY].push_back(interactionObj);
        }
      }
    }
  } catch (json::exception &e) {
    std::cerr << "Error while parsing workflow interactions: " << e.what() << std::endl;
    return false;
  }
  in.close();
  if (valueWritten) {
    std::ofstream out(config::INTERACTION_FILENAME);
    out << std::setw(4) << outJsonObject << std::endl;
  } else {
    std::cerr << "Interaction with given id not found" << std::endl;
  }
  return valueWritten;
}

void ProcessEngine::setDefaultVariables(WorkflowExecution *execution) {
  if (!execution) throw std::invalid_argument("Execution not initialized properly");
  execution->setVariable("TMP_FOLDER", config::TEMPORARY_FOLDER, false);
}

bool ProcessEngine::cancelWorkflow() {
  return signalRunningEngine(SIGTERM);
}

json ProcessEngine::restoreStatus() {
  json result;
  if (!util::fileExists(config::INFO_FILENAME)) {
    throw std::runtime_error("No workflow info available to restore (" + config::INFO_FILENAME + " not accessible)");
  }

  std::ifstream in(config::INFO_FILENAME);
  try {
    in >> result;
  } catch (json::exception &e) {
    throw std::runtime_error("Error while parsing workflow info to restore previous workflow state: " + std::string(e.what()));
  }
  return result;
}

bool ProcessEngine::signalRunningEngine(int signal) {
  json restoredStatusJson;
  try {
    restoredStatusJson = restoreStatus();
  } catch (const std::runtime_error &error) {
    std::cerr << error.what() << std::endl;
    return false;
  }

  pid_t pid_of_running_process_engine;
  if (restoredStatusJson.contains(config::PID_KEY)) {
    pid_of_running_process_engine = restoredStatusJson[config::PID_KEY];
  } else {
    std::cerr << "PID of running process engine not found in workflow info." << std::endl;
    return false;
  }

  try {
    sendSignal(pid_of_running_process_engine, signal);
  } catch (const std::runtime_error &error) {
    std::cerr << "Unable to send signal to running process engine: " << error.what() << std::endl;
    return false;
  }

  return true;
}

void ProcessEngine::sendSignal(pid_t pid, int signal) {
  int status = kill(pid, signal);
  if (status == -1) {     // kill syscall failed
    if (errno == ESRCH) { // no such process
      // the process engine process to send the signal to most likely died/crashed or was killed early
      std::stringstream stream;
      stream << "The process engine with PID " << pid << " does not exist (exited early, crashed or was killed).";
      throw std::runtime_error(stream.str());
    }
  }
}

void ProcessEngine::signalHandler(int signal) {
  if (signal == SIGTERM) {
    LOG("** SIGTERM received, attempting graceful shutdown of the workflow execution **");
    currentExecution->cancel();
  }
}

void ProcessEngine::setPath(const std::string &executionFolder) {
  path = executionFolder;
}
