/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#pragma once

#include <regex>
#include <string>

namespace config {
  // name of the file to store the workflow info in (will be created in the execution directory)
  const std::string INFO_FILENAME = ".workflow-status.json";

  // logging options
  const std::string LOG_FOLDER = "log"; // no '/' expected at the end
  const std::string LOG_FILENAME = "execution.log";
  const std::string COMMAND_LOG_FILENAME = "commands.log";
  const char LOG_SEPARATOR = '-';
  const int LOG_SEPARATOR_NUM_SHORT = 120;
  const int LOG_SEPARATOR_NUM_LONG = 200;

  // shortcuts feature
  const std::string SHORTCUT_FILENAME = ".workflow-shortcuts.json";

  // interaction feature
  const std::string INTERACTION_FILENAME = ".workflow-interactions.json";
  const std::string INTERACTION_ARRAY_KEY = "interactions";

  // continuing workflows
  const std::string NODE_STATES_KEY = "node_states";
  const std::string ITERATION_KEY = "iteration";
  const std::string VARIABLES_KEY = "variables";
  const std::string PID_KEY = "pid";

  // variable feature
  const std::regex VARIABLE_REGEX("\\$\\{([a-zA-Z0-9-_\\.:]+)\\}");
  const std::string VARIABLE_VALUE_UNDEFINED("[UNDEFINED]");

  // temporary folder
  const std::string TEMPORARY_FOLDER = "tmp";

  // workflow analysis
  const int MAX_BRANCH_RECURSION_DEPTH = 10000;
  const int MAX_DEPENDENCY_SCAN_ROUNDS = 10000;

  // loops
  const std::string DEFAULT_LOOP_VARIABLE("index");

  // data and converting
  const std::string TRUE_STRING_REPR(std::to_string(true));   // string used for bool value true internally
  const std::string FALSE_STRING_REPR(std::to_string(false)); // string used for bool value false internally

  const std::vector<std::string> VALID_TRUE_STRING_REPRESENTATIONS({TRUE_STRING_REPR, "true", "True", "TRUE"});
  const std::vector<std::string> VALID_FALSE_STRING_REPRESENTATIONS({FALSE_STRING_REPR, "false", "False", "FALSE"});

  const std::string STDOUT_DATA_KEY = "stdoutData";
} // namespace config
