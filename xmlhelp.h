/* Copyright 2022 Karlsruhe Institute of Technology
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License. */

#pragma once
#include <CLI/CLI.hpp>

std::string convertTypeName(const std::string &typeName) {
  if (typeName == "TEXT") {
    return "string";
  } else if (typeName == "file") {
    return "filein";
  }
  return typeName;
}

void printXMLHelpOption(CLI::Option *option, int &position, const std::string &indent = "  ") {
  std::string name = option->get_name();
  std::string type;
  if (option->get_positional()) {
    name = "arg" + std::to_string(position++);
  }
  if (option->get_expected_min() == 0) {
    type = "flag";
  } else {
    type = convertTypeName(option->get_type_name());
  }
  name.erase(0, std::min(name.find_first_not_of('-'), name.size() - 1));
  std::cout << indent << R"(<param name=")" << name << R"(")";
  if (!option->get_snames().empty()) {
    std::cout << R"( char=")" << option->get_snames().at(0) << R"(")";
  }
  std::cout << R"( type=")" << type << R"(")"
            << R"( description=")" << option->get_description() << R"(")";
  if (option->get_required()) {
    std::cout << R"( required="true")";
  }
  if (option->get_positional()) {
    std::cout << R"( positional="true")";
  }
  std::cout << " />" << std::endl;
}

void printXMLHelp(CLI::App *command, const std::string &versionString) {
  std::cout << R"(<?xml version="1.0" encoding="UTF-8" ?>)" << std::endl;
  std::cout << "<program name=\"process_engine " << command->get_name() << R"(" version=")" << versionString << R"(")"
            << R"( description=")" << command->get_description() << R"(">)" << std::endl;
  int position = 0;
  for (auto option : command->get_options()) {
    if (option->get_name(true) != "help")
      printXMLHelpOption(option, position);
  }
  std::cout << "</program>" << std::endl;
}

void listSubCommands(CLI::App *app, const std::string &executableName) {
  for (auto command : app->get_subcommands(nullptr)) {
    std::cout << executableName << " " << command->get_name() << std::endl;
  }
}
