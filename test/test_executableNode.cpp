/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/WorkflowExecutor.h"
#include "../workflow/nodes/EnvNode.h"
#include "../workflow/nodes/UserInteractionNode.h"
#include <catch2/catch.hpp>

class TestNode : public ExecutableNode {
public:
  explicit TestNode(const std::string &id) : ExecutableNode(id) {
    addPort(std::make_unique<DependencyPort>(0, PortDirection::In));
    addPort(std::make_unique<DependencyPort>(0, PortDirection::Out));
  }
  bool execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) override {
    REQUIRE(executionProfile != ExecutionProfile::SKIP);
    return true;
  }
  const std::string &getName() const override { return name; }

private:
  std::string name = "TestNode";
};

class UITestNode : public UserInteractionNode {
public:
  explicit UITestNode(const std::string &id) : UserInteractionNode(id) {
  }
  bool processInputPortData(WorkflowExecution *) override {
    return true;
  }
  bool processOutputPortData(WorkflowExecution *) override {
    return true;
  }
  bool execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) override {
    REQUIRE(executionProfile != ExecutionProfile::SKIP);
    return UserInteractionNode::execute(execution, dry, detach, iteration);
  }

protected:
  json getInteractionJson(int) const override {
    return {};
  }
  void setInteractionJson(int, json) const override {}

private:
  bool needsInteraction(int, int) const override {
    return true;
  }
  void initInteraction(int, int) const override {}

  const std::string &getName() const override { return name; }

private:
  std::string name = "UITestNode";
};

TEST_CASE("Test execution profiles") {
  SECTION("Skipping nodes based on profile") {
    Workflow workflow;
    auto executedNode = std::make_unique<TestNode>("executed");
    auto skippedNode = std::make_unique<TestNode>("skipped");
    skippedNode->setExecutionProfile(ExecutionProfile::SKIP);
    auto executedNode2 = std::make_unique<TestNode>("executed2");
    workflow.addNode(std::move(executedNode));
    workflow.addNode(std::move(skippedNode));
    workflow.addNode(std::move(executedNode2));
    workflow.connectPorts("executed", 0, "skipped", 0);
    workflow.connectPorts("skipped", 0, "executed2", 0);

    WorkflowExecution testExecution(&workflow);
    REQUIRE(WorkflowExecutor::execute(&workflow, &testExecution, false, false));
  }

  SECTION("Skipping interaction nodes") {
    Workflow workflow;
    auto executedNode = std::make_unique<TestNode>("executed");
    auto skippedUINode = std::make_unique<UITestNode>("skippedUI");
    skippedUINode->setExecutionProfile(ExecutionProfile::SKIP);
    auto executedNode2 = std::make_unique<TestNode>("executed2");
    workflow.addNode(std::move(executedNode));
    workflow.addNode(std::move(skippedUINode));
    workflow.addNode(std::move(executedNode2));
    workflow.connectPorts("executed", 0, "skippedUI", 0);
    workflow.connectPorts("skippedUI", 0, "executed2", 0);

    WorkflowExecution testExecution(&workflow);
    REQUIRE(WorkflowExecutor::execute(&workflow, &testExecution, false, false));
  }

  SECTION("Skipping a single interaction node") {
    Workflow workflow;
    auto skippedUINode = std::make_unique<UITestNode>("skippedUI");
    WorkflowExecution testExecution(&workflow);
    REQUIRE(WorkflowExecutor::execute(&workflow, &testExecution, false, false));
    REQUIRE(WorkflowExecutor::execute(&workflow, &testExecution, false, true));
  }

  /* We need to assure that the mechanism to find and run other reachable UI nodes does not ignore the skipping intension
   *
   *        -- [executed UI node] --
   *      /                         \
   * E -- ---- [skipped UI node] -- -- E2
   *
   * */
  SECTION("Skipping with multiple interaction nodes") {
    Workflow workflow;
    auto executedNode = std::make_unique<TestNode>("executed");
    auto executedUINode = std::make_unique<UITestNode>("executedUI");
    auto skippedUINode = std::make_unique<UITestNode>("skippedUI");
    skippedUINode->setExecutionProfile(ExecutionProfile::SKIP);
    auto executedNode2 = std::make_unique<TestNode>("executed2");
    workflow.addNode(std::move(executedNode));
    workflow.addNode(std::move(executedUINode));
    workflow.addNode(std::move(skippedUINode));
    workflow.addNode(std::move(executedNode2));
    workflow.connectPorts("executed", 0, "skippedUI", 0);
    workflow.connectPorts("executed", 0, "executedUI", 0);
    workflow.connectPorts("skippedUI", 0, "executed2", 0);
    workflow.connectPorts("executedUI", 0, "executed2", 0);

    WorkflowExecution testExecution(&workflow);
    REQUIRE(WorkflowExecutor::execute(&workflow, &testExecution, false, false));
    REQUIRE(WorkflowExecutor::execute(&workflow, &testExecution, false, true));
  }

  SECTION("Skipping EnvNodes") {
    Workflow workflow;
    auto envNode = std::make_unique<EnvNode>("env");
    envNode->addPort(std::make_unique<EnvPort>(0, PortDirection::Out));
    envNode->setExecutionProfile(ExecutionProfile::SKIP);
    auto toolNode = std::make_unique<ToolNode>("tool");
    toolNode->addPort(std::make_unique<EnvPort>(0, PortDirection::In));
    workflow.addNode(std::move(envNode));
    workflow.addNode(std::move(toolNode));
    workflow.connectPorts("env", 0, "tool", 0);

    WorkflowExecution testExecution(&workflow);
    ToolNode *toolNodePtr = dynamic_cast<ToolNode *>(workflow.getNode("tool"));
    REQUIRE(toolNodePtr);
    REQUIRE(toolNodePtr->getCommand(&testExecution).find("env-exec") == std::string::npos);
  }
}
