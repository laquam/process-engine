/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../config.h"
#include "../workflow/Workflow.h"
#include <catch2/catch.hpp>


TEST_CASE("Variable regex") {
  std::cmatch match;
  REQUIRE(std::regex_match("${test}", match, config::VARIABLE_REGEX));
  REQUIRE(match[1] == "test");
  REQUIRE(std::regex_match("${test123}", match, config::VARIABLE_REGEX));
  REQUIRE(match[1] == "test123");
  REQUIRE(std::regex_match("${TeSt123}", match, config::VARIABLE_REGEX));
  REQUIRE(match[1] == "TeSt123");
  REQUIRE(std::regex_search("${test}", config::VARIABLE_REGEX));
  REQUIRE(std::regex_search("This is a ${test}!", config::VARIABLE_REGEX));

  REQUIRE_FALSE(std::regex_match("${TeSt", match, config::VARIABLE_REGEX));
  REQUIRE_FALSE(std::regex_match("{test}", match, config::VARIABLE_REGEX));
  REQUIRE_FALSE(std::regex_match("$test", match, config::VARIABLE_REGEX));
  REQUIRE_FALSE(std::regex_match("$test$", match, config::VARIABLE_REGEX));
  REQUIRE_FALSE(std::regex_match("${te*st}", match, config::VARIABLE_REGEX));
}

TEST_CASE("Expanding of variables") {
  WorkflowExecution execution(nullptr, false, nullptr);
  execution.setVariable("myVar0", "test");
  execution.setVariable("standard_variable", "foobar baz 123");

  // test basic replacement
  REQUIRE(execution.expandVariables("This is a ${myVar0}!") == "This is a test!");

  // multiple variables in one string
  REQUIRE(execution.expandVariables("This is a ${myVar0} with two variables: ${standard_variable}!") == "This is a test with two variables: foobar baz 123!");

  // test updating of a variable
  execution.setVariable("myVar0", "updated test");
  REQUIRE(execution.expandVariables("${myVar0}") == "updated test");

  // test that missing variables are replaced like expected
  REQUIRE(execution.expandVariables("${missing}") == config::VARIABLE_VALUE_UNDEFINED);
  REQUIRE(execution.expandVariables("Some vars might be ${this_was_not_set} after replacement") == "Some vars might be " + config::VARIABLE_VALUE_UNDEFINED + " after replacement");
}

TEST_CASE("Variable restrictions") {
  WorkflowExecution workflow(nullptr, false, nullptr);

  // assure that variable restrictions are in place
  REQUIRE_THROWS(workflow.setVariable("myRecursiveVar", "Value containing ${someOtherVar}."));
  REQUIRE_THROWS(workflow.setVariable("self-contained", "Endless? ${self-contained}"));
}
