/* Copyright 2021 Karlsruhe Institute of Technology
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#include "../workflow/Workflow.h"
#include "../workflow/nodes/FileInputNode.h"
#include "../workflow/nodes/FileOutputNode.h"
#include "../workflow/nodes/ToolNode.h"
#include "../workflow/nodes/control/VariableNode.h"
#include "../workflow/nodes/interaction/UserInputFileNode.h"
#include "../workflow/nodes/interaction/UserOutputTextNode.h"
#include <catch2/catch.hpp>

#include "../workflow/WorkflowAnalyzer.h"
#include "../workflow/nodes/control/IfNode.h"
#include "helpers.h"


TEST_CASE("Sequential execution sets") {

  Workflow workflow;
  /**
  * [A] --- [B]
  *     |
  *     --- [C] --- [D]
  *
  * Expected: {A} -> {B,C} -> {D}
  */

  workflow.addNode(std::make_unique<FileInputNode>("A", "some path"));
  workflow.addNode(std::make_unique<UserInputFileNode>("B", "some description", 0));
  workflow.addNode(std::make_unique<VariableNode>("C"));
  workflow.addNode(std::make_unique<FileOutputNode>("D", "some path", "another path", false));

  workflow.connectPorts("A", 0, "B", 0);
  workflow.connectPorts("A", 0, "C", 0);
  workflow.connectPorts("C", 0, "D", 0);

  workflow.initCoherentGroups();

  SECTION("Simple") {
    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
    auto result = workflowAnalyzer.generateSequentialExecutionSets();
    std::vector<std::unordered_set<std::string>> expected_result{
      {"A"},
      {"B", "C"},
      {"D"}};
    REQUIRE(result == expected_result);
  }

  SECTION("Two possible nodes to start from") {
    /**
    * [A] --- [B]
    *     |
    *     --- [C] --- [D]
    * [E]
    *
    * Expected: {A,E} -> {B,C} -> {D}
    */
    workflow.addNode(std::make_unique<UserOutputTextNode>("E", "some path", 0));
    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
    std::vector<std::unordered_set<std::string>> expected_result{
      {"A", "E"},
      {"B", "C"},
      {"D"}};
    auto result = workflowAnalyzer.generateSequentialExecutionSets();
    REQUIRE(result == expected_result);
  }

  SECTION("Drawback of the set method: Visual dependencies") {
    /**
    * [A] --- [B] --- [F]
    *     |
    *     --- [C] --- [D]
    * [E]
    *
    * Expected: {A,E} -> {B,C} -> {F, D}
    */
    workflow.addNode(std::make_unique<UserOutputTextNode>("E", "some path", 0));
    workflow.addNode(std::make_unique<FileOutputNode>("F", "some path", "", false));
    workflow.connectPorts("B", 0, "F", 0);
    workflow.initCoherentGroups();
    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
    std::vector<std::unordered_set<std::string>> expected_result{
      {"A", "E"},
      {"B", "C"},
      {"D", "F"}};
    auto result = workflowAnalyzer.generateSequentialExecutionSets();
    REQUIRE(result == expected_result);
  }
}

/**
 *                    ----- [E]
 *                   /
 * [A] ----------- [C] @ @ [D]
 *     \          @
 *      -- [B] @ @
 *
 *  (@: pipe connection)
 *  Expected: {A} -> {B,C,D} -> {E}
 *
 * This is because B, C and D have to be executed together and thus share the same dependencies!
 */
TEST_CASE("Sequential execution sets with pipes") {

  Workflow workflow;

  for (const auto &i : {"A", "B", "C", "D", "E"}) {
    workflow.addNode(helpers::createSimpleToolNode(i));
  }

  // dependency connections
  workflow.connectPorts("A", 0, "B", 0);
  workflow.connectPorts("A", 0, "C", 0);
  workflow.connectPorts("C", 0, "E", 0);

  // pipe connections
  workflow.connectPorts("B", 1, "C", 1);
  workflow.connectPorts("C", 1, "D", 1);

  workflow.initCoherentGroups();

  std::vector<std::unordered_set<std::string>> expected_result{
    {"A"},
    {"B", "C", "D"},
    {"E"}};
  auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
  auto result = workflowAnalyzer.generateSequentialExecutionSets();
  REQUIRE(result == expected_result);
}

/**
*                    --------- [E]
*                   /           |
* [A] ----------- [C] -- [D]    |
*     \          /              /
*      -- [B] --  <------------
*
*  (@: pipe connection)
*  Expected: {A} -> {B,C,D} -> {E}
*
* This is because B, C and D have to be executed together and thus share the same dependencies!
*/
TEST_CASE("Sequential execution sets with circle") {
  Workflow workflow;

  for (const auto &i : {"A", "B", "C", "D", "E"}) {
    workflow.addNode(helpers::createSimpleToolNode(i));
  }

  // dependency connections
  workflow.connectPorts("A", 0, "B", 0);
  workflow.connectPorts("A", 0, "C", 0);
  workflow.connectPorts("B", 0, "C", 0);
  workflow.connectPorts("C", 0, "D", 0);
  workflow.connectPorts("C", 0, "E", 0);
  workflow.connectPorts("E", 0, "B", 0); // closing the circle

  workflow.initCoherentGroups();
  auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
  REQUIRE_THROWS_AS(workflowAnalyzer.generateSequentialExecutionSets(), std::logic_error);
  REQUIRE_THROWS_WITH(workflowAnalyzer.generateSequentialExecutionSets(),
                      Catch::Matchers::Contains("Could not resolve graph dependencies"));
}

TEST_CASE("Implicit dependencies (connected with data ports only)") {
  // TODO
}


TEST_CASE("Test coherent node detection") {

  /**
  *                    ----- [E]
  *                   /
  * [A] ----------- [C] @ @ [D]
  *     \          @
  *      -- [B] @ @
  *
  *  (@: pipe connection)
  *
  */
  Workflow workflow;

  for (const auto &i : {"A", "B", "C", "D", "E"}) {
    // add env nodes only for B (needed for the second section of the test)
    workflow.addNode(helpers::createSimpleToolNode(i, std::string("B") == i));
  }
  auto nodes = workflow.getNodes();

  // dependency connections
  workflow.connectPorts("A", 0, "B", 0);
  workflow.connectPorts("A", 0, "C", 0);
  workflow.connectPorts("C", 0, "E", 0);

  // pipe connections
  workflow.connectPorts("B", 1, "C", 1);
  workflow.connectPorts("C", 1, "D", 1);
  SECTION("Detecting a pipe group") {

    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);

    REQUIRE_FALSE(workflowAnalyzer.findCoherentNodeGroup("A"));
    REQUIRE_FALSE(workflowAnalyzer.findCoherentNodeGroup("E"));

    auto detectedPipeNodesB = workflowAnalyzer.findCoherentNodeGroup("B");
    auto detectedPipeNodesC = workflowAnalyzer.findCoherentNodeGroup("C");
    auto detectedPipeNodesD = workflowAnalyzer.findCoherentNodeGroup("D");

    // each node in the "piped" group BCD must know that it is piped together with the others of the group
    // the result of findCoherentNodeGroup must include exactly B, C and D for all three nodes
    int expectedGroupSize = 3;
    REQUIRE(detectedPipeNodesB->size() == expectedGroupSize);
    REQUIRE(detectedPipeNodesC->size() == expectedGroupSize);
    REQUIRE(detectedPipeNodesD->size() == expectedGroupSize);
    REQUIRE(*detectedPipeNodesB == *detectedPipeNodesC);
    REQUIRE(*detectedPipeNodesC == *detectedPipeNodesD);

    std::unordered_set<std::string> resultSet = detectedPipeNodesB->getNodeIds();
    for (const auto &expectedId : {"B", "C", "D"}) {
      REQUIRE(resultSet.find(expectedId) != resultSet.end());
    }
    auto coherentConnections = detectedPipeNodesC->getCoherentNodes();
    REQUIRE(coherentConnections[0].connectionType == CoherentConnectionType::NONE); // first must be NONE
    REQUIRE(coherentConnections[1].connectionType == CoherentConnectionType::PIPE);
    REQUIRE(coherentConnections[2].connectionType == CoherentConnectionType::PIPE);
  }
}

TEST_CASE("Conversion with outgoing Pipe Port") {
  Workflow workflow;

  workflow.addNode(std::make_unique<FileInputNode>("fileIn", ""));
  auto customTestNode = std::make_unique<ToolNode>("test1");
  customTestNode->addPort(std::make_unique<DataPort>(0, PortDirection::In));
  workflow.addNode(std::move(customTestNode));
  workflow.connectPorts("fileIn", 1, "test1", 0);

  SECTION("The converter connection (pipe to non-Pipe) should not be interpreted as a normal pipe connection") {
    WorkflowAnalyzer analyzer(&workflow);
    REQUIRE(!analyzer.findCoherentNodeGroup("fileIn")); // fail if the algorithm detects a coherent group
  }

  WorkflowExecution testExecution(&workflow);
  FileInputNode *fileInputNode = dynamic_cast<FileInputNode *>(workflow.getNode("fileIn"));

  SECTION("FileInputNode respects outgoing connections from it's pipe output port that are connected to non-pipe ports") {
    REQUIRE(fileInputNode->getCommand(&testExecution).empty());
  }
}

TEST_CASE("Ensure that invalid topologies will be detected") {

  SECTION("Connecting pipe AND env ports should result in an error") {
    Workflow workflow;
    workflow.addNode(helpers::createSimpleToolNode("test1", true));
    workflow.addNode(helpers::createSimpleToolNode("test2", true));
    workflow.connectPorts("test1", 1, "test2", 1); // env
    workflow.connectPorts("test1", 1, "test2", 2); // pipe
    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test1"), std::logic_error);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test2"), std::logic_error);
  }

  SECTION("Connecting pipe to multiple targets should result in an error"
          " (this is not possible in the editor") {
    Workflow workflow;
    workflow.addNode(helpers::createSimpleToolNode("test1", true));
    workflow.addNode(helpers::createSimpleToolNode("test2", true));
    workflow.addNode(helpers::createSimpleToolNode("test3", true));
    workflow.connectPorts("test1", 1, "test2", 1);
    workflow.connectPorts("test1", 1, "test3", 1);
    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test1"), std::logic_error);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test2"), std::logic_error);
    REQUIRE_THROWS_AS(workflowAnalyzer.findCoherentNodeGroup("test3"), std::logic_error);
  }
}

TEST_CASE("Test finding of branches ") {

  SECTION("Test if nested branches will are found correctly") {
    Workflow workflow;
    workflow.addNode(std::make_unique<IfNode>("inner_if"));
    workflow.addNode(std::make_unique<IfNode>("outer_if"));
    workflow.addNode(helpers::createSimpleToolNode("node_in_inner_if_branch", false));
    workflow.addNode(helpers::createSimpleToolNode("node_in_outer_if_branch", false));
    workflow.addNode(helpers::createSimpleToolNode("node_outside", true));

    int branchOutIndex = 2; // use the "false" port
    workflow.connectPorts("outer_if", branchOutIndex, "inner_if", 0);
    workflow.connectPorts("inner_if", branchOutIndex, "node_in_inner_if_branch", 0);
    workflow.connectPorts("inner_if", 0, "node_in_outer_if_branch", 0);
    workflow.connectPorts("outer_if", 0, "node_outside", 0);

    auto workflowAnalyzer = WorkflowAnalyzer(&workflow);

    SECTION("Running on a branch with nested branch") {

      /* when run on the inner IfNode, findBranch should return only the contained node plus the closing EndIfNode */
      SECTION("Running on an inner branch") {
        auto innerBranch = workflowAnalyzer.findBranch(&workflow, "inner_if", 2);
        auto innerBranchIds = innerBranch->getNodeIds();
        REQUIRE_FALSE(innerBranchIds.find("node_in_inner_if_branch") == innerBranchIds.end());
        REQUIRE(innerBranchIds.size() == 1);
      }

      SECTION("Running on an BranchingPort that is not connected") {
        auto emptyBranch = workflowAnalyzer.findBranch(&workflow, "outer_if", 1);
        auto emptyBranchIds = emptyBranch->getNodeIds();
        REQUIRE(emptyBranchIds.empty());
        REQUIRE(emptyBranch->isEmpty());
      }
    }
  }
}
