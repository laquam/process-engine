#include "../config.h"
#include "../workflow/Workflow.h"
#include "../workflow/WorkflowExecutor.h"
#include "../workflow/nodes/control/IfNode.h"
#include "../workflow/nodes/control/LoopNode.h"
#include "../workflow/nodes/source/IntegerSourceNode.h"
#include "../workflow/nodes/source/StringSourceNode.h"
#include <catch2/catch.hpp>
#include <memory>
#include <utility>

class LoopTestNode : public Node {
public:
  explicit LoopTestNode(std::string id, std::vector<int> expectedIndices = {},
                        std::string indexVariableName = config::DEFAULT_LOOP_VARIABLE)
      : Node(std::move(id)), executionCounter(0), name("CountingTestNode"), expectedIndices(std::move(expectedIndices)),
        indexVariableName(std::move(indexVariableName)) {
    createPort<DependencyPort>(0, PortDirection::In);
    indexInPort = createPort<DataPort>(1, PortDirection::In);
  };
  bool execute(WorkflowExecution *execution, bool dry, bool detach, int iteration) override {
    executionCounter++;
    testLoopIndex(execution);
    execution->setNodeExecutionState(getId(), NodeExecutionState::EXECUTED);
    return true;
  };
  void testLoopIndex(WorkflowExecution *execution) {
    int loopIteration = execution->getLoopIteration();
    int expectedIndex = loopIteration;

    // if there was a (customized) list of expected indices specified in constructor, use them
    if (expectedIndices.size() > loopIteration) {
      expectedIndex = expectedIndices[loopIteration];
    }

    // test loop index output port provides the correct value
    REQUIRE(indexInPort->getData());
    REQUIRE(indexInPort->getData()->toInt() == expectedIndex);
    // test loop index variable (${index}) holds the correct value
    REQUIRE(execution->expandVariables("${" + indexVariableName + "}") ==
            std::to_string(expectedIndex));
  }
  unsigned int getExecutionCount() const {
    return executionCounter;
  }
  const std::string &getName() const override {
    return name;
  };

private:
  unsigned int executionCounter;
  DataPort *indexInPort;
  std::string name;
  std::vector<int> expectedIndices;
  std::string indexVariableName;
};

void prepareLoopWorkflow(Workflow &workflow, int startIndex, int endIndex, std::vector<int> expectedIndices = {}) {
  workflow.addNode(std::make_unique<LoopNode>("testLoopNode"));
  workflow.addNode(std::make_unique<LoopTestNode>("testCountingNode", std::move(expectedIndices)));
  workflow.connectPorts("testLoopNode", 1, "testCountingNode", 0);

  // to read the loop index index
  workflow.connectPorts("testLoopNode", 2, "testCountingNode", 1);

  workflow.addNode(std::make_unique<IntegerSourceNode>("startInputNode", std::to_string(startIndex)));
  workflow.addNode(std::make_unique<IntegerSourceNode>("endInputNode", std::to_string(endIndex)));
  workflow.connectPorts("startInputNode", 0, "testLoopNode", 2);
  workflow.connectPorts("endInputNode", 0, "testLoopNode", 3);
}

TEST_CASE("Basic loop test") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 0, 3);
  WorkflowExecution testExecution(&testWorkflow);
  WorkflowExecutor::execute(&testWorkflow, &testExecution, false, false, true);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  SECTION("Test number of loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == 4);
  }
  SECTION("Test counter in NodeState") {
    REQUIRE(testExecution.getNodeState("testCountingNode").getSuccessfulExecutions() == 4);
  }
  SECTION("Test loopIndex variable correctly set") {
    REQUIRE(testExecution.expandVariables("${" + config::DEFAULT_LOOP_VARIABLE + "}") == std::to_string(3));
  }
}

TEST_CASE("Loops with start index = end index should run once") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 3, 3, {3});
  WorkflowExecution testExecution(&testWorkflow);
  WorkflowExecutor::execute(&testWorkflow, &testExecution, false, false, true);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  SECTION("Number of executions should be 1") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == 1);
  }
}

TEST_CASE("Loop within branch") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 0, 3);
  // put the whole test workflow into a branch (connect it to the false port, so that we don't need to set the condition to true)
  testWorkflow.addNode(std::make_unique<IfNode>("testIfNode"));
  testWorkflow.connectPorts("testIfNode", 2, "testLoopNode", 0);
  WorkflowExecution testExecution(&testWorkflow);
  WorkflowExecutor::execute(&testWorkflow, &testExecution, false, false, true);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  SECTION("Test number of loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == 4);
  }
  SECTION("Test counter in NodeState") {
    REQUIRE(testExecution.getNodeState("testCountingNode").getSuccessfulExecutions() == 4);
  }
  SECTION("Test loopIndex variable correctly set") {
    REQUIRE(testExecution.expandVariables("${" + config::DEFAULT_LOOP_VARIABLE + "}") == std::to_string(3));
  }
}

TEST_CASE("Loop within loop") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 0, 3); // this will be the inner loop

  // put the whole test workflow into a branch (connect it to the false port, so that we don't need to set the condition to true)
  testWorkflow.addNode(std::make_unique<LoopNode>("testOuterLoopNode"));
  testWorkflow.connectPorts("testOuterLoopNode", 1, "testLoopNode", 0);

  // parameterize outer loop node
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("outerStartInputNode", std::to_string(0)));
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("outerEndInputNode", std::to_string(8)));
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("outerStepInputNode", std::to_string(2)));
  testWorkflow.addNode(std::make_unique<StringSourceNode>("outerIndexVariableInputNode", std::to_string(2)));
  testWorkflow.addNode(std::make_unique<StringSourceNode>("outerIndexVariableInputNode", "outerIndex"));
  testWorkflow.connectPorts("outerStartInputNode", 0, "testOuterLoopNode", 2);
  testWorkflow.connectPorts("outerEndInputNode", 0, "testOuterLoopNode", 3);
  testWorkflow.connectPorts("outerStepInputNode", 0, "testOuterLoopNode", 4);
  testWorkflow.connectPorts("outerIndexVariableInputNode", 0, "testOuterLoopNode", 5);

  // add another counter node to the outer loop branch (next to the inner loop) in order to verify the number of iterations of the outer loop
  testWorkflow.addNode(std::make_unique<LoopTestNode>("outerCountingNode", std::vector<int>({0, 2, 4, 6, 8}), "outerIndex"));
  testWorkflow.connectPorts("testOuterLoopNode", 1, "outerCountingNode", 0);
  testWorkflow.connectPorts("testOuterLoopNode", 2, "outerCountingNode", 1);

  WorkflowExecution testExecution(&testWorkflow);
  WorkflowExecutor::execute(&testWorkflow, &testExecution, false, false, true);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  int expectedOuterCount = 5; // from 0 to 8 with step 2
  int expectedInnerCount = 4 * expectedOuterCount;
  SECTION("Correct number of inner loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == expectedInnerCount);
  }

  SECTION("Correct number of outer loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("outerCountingNode"));
    REQUIRE(testNode->getExecutionCount() == expectedOuterCount);
  }
  SECTION("Counter in NodeState correct for outer loop") {
    REQUIRE(testExecution.getNodeState("outerCountingNode").getSuccessfulExecutions() == expectedOuterCount);
  }
  SECTION("LoopIndex variable correctly set for outer loop") {
    REQUIRE(testExecution.expandVariables("${outerIndex}") == std::to_string(2 * (expectedOuterCount - 1)));
  }
}

TEST_CASE("Assure that LoopNodes can handle input with variables") {
  Workflow testWorkflow;
  testWorkflow.addNode(std::make_unique<LoopNode>("testLoopNode"));
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("testSource", "${testVar}"));
  testWorkflow.connectPorts("testSource", 0, "testLoopNode", 3);

  WorkflowExecution testExecution(&testWorkflow);
  testExecution.setVariable("testVar", "1");
  REQUIRE(WorkflowExecutor::execute(&testWorkflow, &testExecution, false, false, true));
}

TEST_CASE("Test if step works correctly") {
  Workflow testWorkflow;
  prepareLoopWorkflow(testWorkflow, 5, 25, {5, 10, 15, 20, 25});
  testWorkflow.addNode(std::make_unique<IntegerSourceNode>("stepSourceNode", std::to_string(5)));
  testWorkflow.connectPorts("stepSourceNode", 0, "testLoopNode", 4);
  WorkflowExecution testExecution(&testWorkflow);
  WorkflowExecutor::execute(&testWorkflow, &testExecution, false, false, true);
  REQUIRE(testExecution.getState() == ExecutionState::FINISHED);

  SECTION("Test number of loop iterations") {
    auto testNode = dynamic_cast<const LoopTestNode *>(testWorkflow.getNodeConst("testCountingNode"));
    REQUIRE(testNode->getExecutionCount() == 5);
  }
  SECTION("Test counter in NodeState") {
    REQUIRE(testExecution.getNodeState("testCountingNode").getSuccessfulExecutions() == 5);
  }
  SECTION("Test loopIndex variable correctly set") {
    REQUIRE(testExecution.expandVariables("${" + config::DEFAULT_LOOP_VARIABLE + "}") == std::to_string(25));
  }
}
